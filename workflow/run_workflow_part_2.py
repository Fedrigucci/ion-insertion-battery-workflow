#===================================================================
#         RUN THE SECOND WORKFLOW PART (NEB + POTENTIAL)
#===================================================================
import os
from contextlib import contextmanager
import subprocess
import glob
from shutil import copy2

@contextmanager
def cd(newdir):
    ''' Change working directory and catch errors, dont create any '''
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def create_venv_symlink(material_home_folder):
    if os.path.exists('./venv'):
        src = '/'.join((os.getcwd(), 'venv'))
        dest = '/'.join((material_home_folder, 'venv'))
        if not os.path.exists(dest):
            os.symlink(src, dest)

material_ids_reflective = ['mp-676282', # Chevrel
                           'mp-27872', # MgTi2O4
                           #'mp-1103557',
                           'mp-18900', # MgV2O4
                           'mp-32006', #MgMn2O4
                           #'mp-1192819',
                           #'mp-19573',
                           #'mp-19202', # MgCr2O4
                           #'mp-32432',
                           #'mp-19574',
                           'mp-19541', # Mg3Mn2O12Si3
                           'mp-19581', # Mg3Cr2O12Si3
                           #'mp-1196909'
                           ] # Fe2MgBO5 #,'mp-32432'] # mp-32432 is Mg4V2O8, 'mp-676282' is Chevrel

ion = 'Mg' # which ion will be substituted

material_ids = material_ids_reflective

for material_id in material_ids:
    # make sure workflow_potneb.py is latest one, therefore copy it into directory
    material_home_folder = './' + glob.glob(f'calculations/{ion}/*{material_id}/')[0]
    copy2('workflow_potneb.py', material_home_folder)
    create_venv_symlink(material_home_folder)    
    with cd(material_home_folder):
        subprocess.run('mq workflow workflow_potneb.py', shell = True)
