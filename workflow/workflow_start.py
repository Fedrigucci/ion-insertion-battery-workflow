# =============================================================
#           WORKFLOW SCRIPT FOR ION-CATHODE-MATERIALS
# =============================================================

import shutil
import json

from myqueue.task import task
from iiwPBEU.input import UserInput, FolderStructure

# run multiple structures
multiple = True  # set True for running multiple materials
if multiple:
    with open('current_material.json') as f:
        user_args = json.load(f)
else:
    # run a specific material
    pass

# validate user input
user_args = UserInput.read_user_input(gobacknfolders=0)
UserInput(wf_input=user_args).validate()

# create folder structure
folder_structure = FolderStructure(wf_input=user_args)
f_paths = folder_structure.mkdirs_wf_start()
material_home_folder, bulk_rx, empty_bulk_rx, empty_bulk_scaled_rx = f_paths

def create_tasks():
    tasks = []
    t1 = task('iiwPBEU.relax_bulk_vasp', resources='40:xeon40:1:12h', folder=bulk_rx)
    t2 = task('iiwPBEU.relax_empty_vasp', resources='40:xeon40:1:24h', folder=empty_bulk_rx)
    t3 = task('iiwPBEU.check_stability', resources= '8:xeon8:1:10m', folder=material_home_folder, deps=[t1,t2])
    t4 = task('iiwPBEU.prepare_potneb_vasp', resources='24:xeon24:1:10m', folder=material_home_folder, deps=[t3])
    t5 = task('iiwPBEU.relax_empty_scaled_vasp', resources='40:xeon40:1:12h', folder=empty_bulk_scaled_rx, deps=[t3])
    tasks = [t1, t2, t3, t4, t5]
    return tasks
