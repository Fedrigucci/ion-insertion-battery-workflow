""" Quick and easy tests to verify myqueue """
import os
from shutil import copy2
import subprocess
import json

from ase.db import connect
from ase.build import molecule
from ase.io import write

from iiwPBEU.system_utils import cd, get_wd
from iiwPBEU.input import UserInput, FolderStructure

def test_import_myqueue():
    from myqueue.task import task
    
def test_myqueue_submit(tmp_path):
    current_mat_src = os.path.abspath('current_material.json')
    with open(current_mat_src) as f:
        user_args = json.load(f)
    material_id = user_args["material_id"]
    potneb_dic_src = os.path.abspath('potneb_dictionary.json')
    current_wf_script_1 = os.path.abspath('../workflow/workflow_start.py')
    current_wf_script_2 = os.path.abspath('../workflow/workflow_potneb.py')
    mq_config_file = os.path.abspath('./myqueue_files/config.py')
    d = tmp_path / "starting_structures"; d.mkdir()
    with cd(str(tmp_path.absolute())):

        for f in [current_mat_src,
                  potneb_dic_src,
                  current_wf_script_1,
                  current_wf_script_2]:
            copy2(f, '.')
            
        wd = get_wd([FolderStructure.starting_structures])
        for db in FolderStructure.expected_starting_dbs:
            test = connect('/'.join((wd, db)))
            test.write(molecule('H2'),
                       material_id=user_args["material_id"],
                       with_u_corr=user_args["with_u_corr"],
                       elements='H')

        # initialize myqueue
        subprocess.call('mq init', shell=True)
        dot_mq = get_wd(['.myqueue'])
        with cd(dot_mq):
            copy2(mq_config_file, '.')
   
        # test submit single
        cmd_single = 'mq submit workflow_start.py -R 8:xeon8:1:1h --dry-run -v'
        out = subprocess.check_output(cmd_single, shell=True)
        out = out.splitlines()[-1].decode("utf-8")
        assert str(out) == "1 task to submit"
        
        # test submit full workflow
        # test first part workflow submit
        cmd_wf_1 = 'mq workflow workflow_start.py --dry-run'
        out = subprocess.check_output(cmd_wf_1, shell=True)
        out = out.splitlines()[-1].decode("utf-8")
        assert  str(out) == "5 tasks to submit"
        
        # test second part submit
        molecule('H2').write('supercell.traj')
        cmd_wf_2 = 'mq workflow workflow_potneb.py --dry-run'
        out = subprocess.check_output(cmd_wf_2, shell=True)
        out = out.splitlines()[-1].decode("utf-8")
        assert  str(out) == "9 tasks to submit"
        
