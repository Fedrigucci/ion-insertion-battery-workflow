"""
Tests aiming at checking if workflow tasks work

@author: Felix Bölle <feltbo@dtu.dk>
"""
import os
from shutil import copy2

from iiwPBEU.tasks_neb import TaskRMINEB, TaskCIRNEB, TaskCNEB, TaskCICNEB
from iiwPBEU.system_utils import cd, get_wd
from iiwPBEU.neb.neb_path import NEBPath
from iiwPBEU.input import UserInput

class TaskRMINEB_EMT(TaskRMINEB):
    _hf_material_nback = 0
    def __init__(self, *args, **kwargs):
        super(TaskRMINEB_EMT, self).__init__(*args, **kwargs)
        
    def retrieve_information(self):
        self.magstate='NM'
        self.init_ind = 0
        self.final_ind = 2
        self.remove_n_ions = 1
        self.N_ions = 4
        self.sym_dic = {'symmetric' : True}
        self.potneb_dic = {'repeat_unitcell':[1,1,1],
                           'all_ion_indices':[0,1,2,3],
                           'equal_to':[0, 0, 0, 0]}
        
    def get_test_structure(self):
        from ase.build import fcc111
        test_structure = fcc111('Cu', [2,2,1], 4, periodic=True)
        return test_structure

class TaskCIRNEB_EMT(TaskCIRNEB):
    _hf_material_nback = 0
    def __init__(self, *args, **kwargs):
        super(TaskCIRNEB_EMT, self).__init__(*args, **kwargs)
        
    def retrieve_information(self):
        self.magstate='NM'
        self.init_ind = 0
        self.final_ind = 2
        self.remove_n_ions = 1
        self.N_ions = 4
        self.sym_dic = {'symmetric' : True}
        self.potneb_dic = {'repeat_unitcell':[1,1,1],
                           'all_ion_indices':[0,1,2,3],
                           'equal_to':[0, 0, 0, 0]}
        
    def get_test_structure(self):
        from ase.build import fcc111
        test_structure = fcc111('Cu', [2,2,1], 4, periodic=True)
        return test_structure
    
class TaskCNEB_EMT(TaskCNEB):
    _hf_material_nback = 3
    def __init__(self, *args, **kwargs):
        super(TaskCNEB_EMT, self).__init__(*args, **kwargs)
        
    def retrieve_information(self):
        self.magstate='NM'
        self.init_ind = 0
        self.final_ind = 2
        self.remove_n_ions = 1
        self.N_ions = 4
        self.sym_dic = {'symmetric' : True}
        self.potneb_dic = {'repeat_unitcell':[1,1,1],
                           'all_ion_indices':[0,1,2,3],
                           'equal_to':[0, 0, 0, 0]}
        
    def get_test_structure(self):
        from ase.build import fcc111
        test_structure = fcc111('Cu', [2,2,1], 4, periodic=True)
        return test_structure
    
class TaskCICNEB_EMT(TaskCICNEB):
    _hf_material_nback = 3
    def __init__(self, *args, **kwargs):
        super(TaskCICNEB_EMT, self).__init__(*args, **kwargs)
        
    def retrieve_information(self):
        self.magstate='NM'
        self.init_ind = 0
        self.final_ind = 2
        self.remove_n_ions = 1
        self.N_ions = 4
        self.sym_dic = {'symmetric' : True}
        self.potneb_dic = {'repeat_unitcell':[1,1,1],
                           'all_ion_indices':[0,1,2,3],
                           'equal_to':[0, 0, 0, 0]}
        
    def get_test_structure(self):
        from ase.build import fcc111
        test_structure = fcc111('Cu', [2,2,1], 4, periodic=True)
        return test_structure

class TestNEBs:
    """ Test the different NEB tasks and anything related to it """
    
    def create_empty_cell(self, atoms, tmp_path, task):
        from ase.io import write
        # create needed directories
        d = tmp_path / "bulk_empty"; d.mkdir()
        d = d / task.magstate; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        
        # empty cell
        with cd(str(d.absolute())):
            atoms_empty = atoms.copy()
            scaled_cell = atoms_empty.get_cell() * 0.9**(1/3)
            atoms_empty.set_cell(scaled_cell, scale_atoms=True)
            write('test.traj', atoms_empty)
            
    def create_relaxed_potential_structure(self,
        task, ion_ind, atoms, tmp_path, 
        write_to_wavecar=None, write_to_outcar=None, return_path=False):
        from ase.io import write
        from ase.calculators.singlepoint import SinglePointCalculator
        f_name = f'{task.remove_n_ions}_{task.N_ions}_{ion_ind}'
        # create needed directories
        try: 
            d = tmp_path / "potential"; d.mkdir()
        except FileExistsError: pass;
        d = d / f_name; d.mkdir()
        d = d / task.magstate; d.mkdir()
        d = d / "vasp_rx_PBESOL"; d.mkdir()
        
        # empty cell
        with cd(str(d.absolute())):
            atoms_empty = atoms.copy()
            del atoms_empty[ion_ind]
            results = {'energy': -2}
            atoms_empty.set_calculator(
                SinglePointCalculator(atoms_empty, **results))
            write('test.traj', atoms_empty)
            # write_stuff to file if wanted
            if write_to_wavecar:
                with open('WAVECAR', 'w') as f:
                    f.write(write_to_wavecar)
            if write_to_outcar:
                with open('OUTCAR', 'w') as f:
                    f.write(write_to_outcar)
        if return_path:
            return str(d.absolute())
        
    def test_rmi_neb(self, tmp_path):
        from ase.io import write
        import json
        emt_task = TaskRMINEB_EMT()
        atoms = emt_task.get_test_structure()
        emt_task.retrieve_information()
        
        self.create_empty_cell(atoms, tmp_path, emt_task)
        
            
        with cd(str(tmp_path.absolute())):
            # supercell
            write('supercell.traj', atoms)
        
            init = atoms.copy()
            del init[emt_task.init_ind]
            final = atoms.copy()
            del final[emt_task.final_ind]
            # get symmetry.json file
            from iiwPBEU.neb.check_reflective import is_reflective
            is_reflective(atoms, init, final)
         
            images = emt_task.get_images(init, atoms_f='test.traj',
                                         restart_f=None)

            images = emt_task.launch_NEB(N=emt_task.N,
                                         magstate='NM',
                                         RMI_NEB=True,
                                         EMT_test=True,
                                         tol=emt_task._sym_tol)
            
            assert len(images) == 3
    
    def test_cir_neb_input(self, tmp_path):
        """ check that threshold is read correctly """
        from iiwPBEU.battery_tools import get_current_material_dic
        from shutil import copy2
        import os
        from iiwPBEU.tasks_neb import TaskCIRNEB
        from iiwPBEU.relax_cirneb_vasp import get_barrier_threshold
        current_mat_src = os.path.abspath('current_material.json')
        
        with cd(str(tmp_path.absolute())):
            mat_home_folder = get_wd([
                'calculations', 'Cu', 'Cu_test'])
            with cd(mat_home_folder):
                copy2(current_mat_src, '.')
                cirneb_folder = get_wd([
                'NEBs', '0_1_1_4', 'CIRNEB', 'FM', 'vasp_rx_PBESOL'])
                with cd(cirneb_folder):
                    bar = get_barrier_threshold(
                        gobacknfolders=TaskCIRNEB._hf_material_nback)
                    task = TaskCIRNEB(barrier_threshold=bar)
                    assert bar == task.barrier_threshold

    def test_cir_neb(self, tmp_path):
        """ check that launch_NEB/get_images work with EMT """
        from ase.io import write
        from ase.calculators.singlepoint import SinglePointCalculator
        
        emt_task = TaskCIRNEB_EMT()
        emt_task.retrieve_information()
        atoms = emt_task.get_test_structure()
        self.create_empty_cell(atoms, tmp_path, emt_task)
            
        with cd(str(tmp_path.absolute())):
            # supercell
            write('supercell.traj', atoms)
            emt_task.retrieve_information()
        
            init = atoms.copy()
            del init[emt_task.init_ind]
            final = atoms.copy()
            del final[emt_task.final_ind]
            images = [init, final]
            from iiwPBEU.neb.check_reflective import is_reflective
            is_reflective(atoms, init, final)
            
            results = {'energy': -2}
            images[0].set_calculator(SinglePointCalculator(images[0], **results))
            # create the path
            neb_path = NEBPath(tol=1e-3)
            # align indices
            final, _ = neb_path.align_indices(images[0], images[1], final=None)
            final.set_calculator(SinglePointCalculator(final, **results))
            images = [images[0], final]

            images = emt_task.get_images(rmi_images=images)

            emt_task._fmax = 0.1 # faster convergence
            images = emt_task.launch_NEB(N=emt_task.N,
                                         magstate='NM',
                                         EMT_test=True,
                                         tol=emt_task._sym_tol)

            assert len(images) == emt_task.N
            
    def test_cir_neb_below_thresh(self, tmp_path):
        """ Check if TS already found through RMINEB works """
        from ase.io import write, read
        from ase.calculators.singlepoint import SinglePointCalculator
        
        emt_task = TaskCIRNEB_EMT()
        emt_task.retrieve_information()
        atoms = emt_task.get_test_structure()
        self.create_empty_cell(atoms, tmp_path, emt_task)
            
        with cd(str(tmp_path.absolute())):
            # supercell
            write('supercell.traj', atoms)
            emt_task.retrieve_information()
        
            init = atoms.copy()
            del init[emt_task.init_ind]
            final = atoms.copy()
            del final[emt_task.final_ind]
            images = [init, final]
            from iiwPBEU.neb.check_reflective import is_reflective
            is_reflective(atoms, init, final)
            
            images[0].set_calculator(SinglePointCalculator(
                images[0], **{'energy': -2}))
            # create the path
            neb_path = NEBPath(tol=1e-3)
            # align indices
            final, _ = neb_path.align_indices(images[0], images[1], final=None)
            final.set_calculator(SinglePointCalculator(
                final, **{'energy': 3}))
            images = [images[0], final]

            images = emt_task.get_images(rmi_images=images)

            emt_task._fmax = 0.1 # faster convergence
            images = emt_task.launch_NEB(N=emt_task.N,
                                         magstate='NM',
                                         EMT_test=True,
                                         tol=emt_task._sym_tol)

            assert emt_task.check_images_below_is_fs(images) == True
            assert len(images) == emt_task.N
            # check that the trajectory written is not empty!
            images_read = read('neb.traj@:')
            assert len(images_read) == emt_task.N
            
    def test_c_neb(self, tmp_path):
        from ase.io import write
        from shutil import copy2
        emt_task = TaskCNEB_EMT()
        emt_task.retrieve_information()
        atoms = emt_task.get_test_structure()
        self.create_empty_cell(atoms, tmp_path, emt_task)

        self.create_relaxed_potential_structure(
            emt_task, emt_task.init_ind, atoms, tmp_path)
        self.create_relaxed_potential_structure(
            emt_task, emt_task.final_ind, atoms, tmp_path)

        with cd(str(tmp_path.absolute())):
            # supercell
            write('supercell.traj', atoms)
            d = tmp_path / "CNEB"; d.mkdir()
            d = d / "NM"; d.mkdir()
            d = d / "vasp_rx_PBESOL"; d.mkdir()

            with cd(str(d.absolute())):
                init = atoms.copy()
                del init[2]
                final = atoms.copy()
                del final[0]
                images = [init, final]
                from iiwPBEU.neb.check_reflective import is_reflective
                is_reflective(atoms, init, final)
                
                images = emt_task.get_images(atoms_f='test.traj', restart_f=None)
                # check that initial and final image have energy assigned
                assert isinstance(images[0].calc.results['energy'], (int,float))
                assert isinstance(images[-1].calc.results['energy'], (int,float))
                emt_task._fmax = 0.1 # faster convergence
                # check that init_path is there
                ip_cneb = ('/').join(emt_task.init_path.split('/')[-4:])
                ip_expected = 'potential/1_4_0/NM/vasp_rx_PBESOL'
                assert ip_cneb == ip_expected
                
                images = emt_task.launch_NEB(N=emt_task.N,
                                             magstate='NM',
                                             EMT_test=True,
                                             tol=emt_task._sym_tol)
                es_cneb = [atoms.get_potential_energy() for atoms in images]
    
                assert len(images) == emt_task.N
                assert isinstance(images[0].calc.results['energy'], (int,float))
                assert isinstance(images[-1].calc.results['energy'], (int,float))

            # now the CICNEB
            emt_task = TaskCICNEB_EMT()
            d = tmp_path / "CICNEB"; d.mkdir()
            d = d / "NM"; d.mkdir()
            d = d / "vasp_rx_PBESOL"; d.mkdir()
            
            with cd(str(d.absolute())):
                emt_task.copy_symmetry_dic()
                emt_task.retrieve_information()
                images = emt_task.get_images(atoms_f='test.traj', restart_f=None)
                ip_cicneb = ('/').join(emt_task.init_path.split('/')[-4:])
                assert ip_cicneb == ip_expected
                assert emt_task.final_path
                
                assert isinstance(images[0].calc.results['energy'], (int,float))
                assert isinstance(images[-1].calc.results['energy'], (int,float))
                emt_task._fmax = 0.09 # faster convergence
                images = emt_task.launch_NEB(N=emt_task.N,
                                             magstate='NM',
                                             EMT_test=True,
                                             tol=emt_task._sym_tol)
                assert len(images) == emt_task.N
                assert isinstance(images[0].calc.results['energy'], (int,float))
                assert isinstance(images[-1].calc.results['energy'], (int,float))
                es_cicneb = [atoms.get_potential_energy() for atoms in images]
                
                # check climbing image doing something
                # TODO: something is off, since this test is not parsing!?
                # Or is it just this test system
                # assert max(es_cicneb) > max(es_cneb)

    
    def test_c_neb_initialization(self, tmp_path):
        from ase.calculators.vasp import Vasp2
        from ase.io import write
        emt_task = TaskCNEB_EMT()
        emt_task._hf_material_nback = 0
        emt_task.retrieve_information()
        emt_task.magstate = 'FM'
        atoms = emt_task.get_test_structure()
        self.create_empty_cell(atoms, tmp_path, emt_task)
        
        init_path = self.create_relaxed_potential_structure(
            emt_task, emt_task.init_ind, atoms, tmp_path,
            write_to_wavecar='initial',
            write_to_outcar='number of electron 1 magnetization 5.0',
            return_path=False)
        emt_task.init_path = init_path
        final_path = self.create_relaxed_potential_structure(
            emt_task, emt_task.final_ind, atoms, tmp_path,
            write_to_wavecar='final',
            write_to_outcar='number of electron 1 magnetization 13.0',
            return_path=False)
        emt_task.final_path = final_path
            
        with cd(str(tmp_path.absolute())):
            # supercell
            write('supercell.traj', atoms)
        
            init = atoms.copy()
            del init[2]
            final = atoms.copy()
            del final[0]
            images = [init, final]
            from iiwPBEU.neb.check_reflective import is_reflective
            is_reflective(atoms, init, final)
            
            emt_task.N = 9
            images = emt_task.get_images(atoms_f='test.traj', restart_f=None)
            # pass calculator
            calc = Vasp2()
            # that is the only function that is not tested in the EMT case
            emt_task.setup_calculator_images(images,
                                     t=emt_task.N-1,
                                     calc=calc,
                                     magstate=emt_task.magstate,
                                     init_path=init_path,
                                     EMT_test=False)
            with open('image_3/WAVECAR', 'r') as f:
                assert f.readlines()[0] == 'initial'
            with open('image_4/WAVECAR', 'r') as f:
                assert f.readlines()[0] == 'final'
                
            # check magmom on images
            assert images[1].calc.int_params['nupdown'] == 6
            assert images[-2].calc.int_params['nupdown'] == 12
            
class TestRelax:
    """ Test all the relax tasks """
    def test_relax_bulk(self, tmp_path):
        from iiwPBEU.relax_bulk_vasp import TaskRelaxBulk
        user_args = UserInput.read_user_input(0)
        user_id = user_args["material_id"]
        current_mat_src = os.path.abspath('current_material.json')
        copy2(current_mat_src, str(tmp_path.absolute()))
        # create needed directories
        d = tmp_path / "bulk"; d.mkdir()
        d = d / "NM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        # empty cell
        with cd(str(d.absolute())):
            
            task = TaskRelaxBulk()
            task.retrieve_information()
            assert task.material_id == user_id
            
    def test_relax_bulk_empty(self, tmp_path):
        from iiwPBEU.relax_empty_vasp import TaskRelaxBulkEmpty
        user_args = UserInput.read_user_input(0)
        user_id = user_args["material_id"]
        current_mat_src = os.path.abspath('current_material.json')
        copy2(current_mat_src, str(tmp_path.absolute()))
        # create needed directories
        d = tmp_path / "bulk_empty"; d.mkdir()
        d = d / "NM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        # empty cell
        with cd(str(d.absolute())):
            task = TaskRelaxBulkEmpty()
            task.retrieve_information()
            assert task.material_id == user_id
            
    def test_relax_bulk_empty_scaled(self, tmp_path):
        from iiwPBEU.relax_empty_scaled_vasp import TaskRelaxBulkEmptyScaled
        user_args = UserInput.read_user_input(0)
        user_id = user_args["material_id"]
        current_mat_src = os.path.abspath('current_material.json')
        copy2(current_mat_src, str(tmp_path.absolute()))
        # create needed directories
        d = tmp_path / "bulk_empty_scaled"; d.mkdir()
        d = d / "NM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        # empty cell
        with cd(str(d.absolute())):
            task = TaskRelaxBulkEmptyScaled()
            task.retrieve_information()
            assert task.material_id == user_id
    
    def test_relax_bulk_potential(self, tmp_path):
        from iiwPBEU.relax_potential_vasp import TaskRelaxBulkPotential
        user_args = UserInput.read_user_input(0)
        user_id = user_args["material_id"]
        current_mat_src = os.path.abspath('current_material.json')
        copy2(current_mat_src, str(tmp_path.absolute()))
        # create needed directories
        try: 
            d = tmp_path / "potential"; d.mkdir()
        except FileExistsError: pass;
        d = d / "1_4_0"; d.mkdir()
        d = d / "NM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        # empty cell
        with cd(str(d.absolute())):
            task = TaskRelaxBulkPotential()
            task.retrieve_information()
            assert task.material_id == user_id


from iiwPBEU.prepare_potneb_vasp import TaskPreparePotneb
import json

class TaskPreparePotnebTest(TaskPreparePotneb):
    _hf_material_nback = 0
    def __init__(self, *args, **kwargs):
        super(TaskPreparePotnebTest, self).__init__(*args, **kwargs)
        
    def retrieve_information(self):
        with open('current_material.json') as f:
            user_args = json.load(f)
            
        user_args['cut_off_neb'] = 3
        user_args['ion'] = "Cu"
        user_args['remove_n_ions'] = "single_one"
        
        return user_args
    
    def get_atoms_object(self, magstate):
        from ase.build import fcc111
        # get starting structure for symmetry inequivalent finder
        # Here we use the relaxed unit cell to do symmetry analysis on
        atoms = fcc111('Cu', [2,1,1], a=4, periodic=True)
        # also get empty supercell for reflection check
        uc_empty = fcc111('Cu', [2,1,1], a=3, periodic=True)
        
        return atoms, uc_empty

class TestPreparePotneb:
    """ any heavy functions related to the prepare potneb script """
    def test_choose_reflective_path(self):
        """ test that it chooses the reflective path """
        from ase.build import fcc111
        from iiwPBEU.inequivalent_site_finder import InequivalentSiteFinder
        from iiwPBEU.prepare_potneb_vasp import choose_reflective_path
        # prepare test input structure
        # this structure has equiv paths with and without reflection symmetry!
        atoms = fcc111('Cu', [2,2,2], a=4, periodic=True)
        n_atoms_2 = int(len(atoms)/2)
        atoms.set_chemical_symbols(['Cu']*n_atoms_2 + ['Ni']*n_atoms_2)
        # siteFinder test also for different vacancy spacing
        siteFinder = InequivalentSiteFinder(
            atoms=atoms,
            ion='Cu',
            cutoff_neb_images = 2.9,
            min_vac_spacing=6)
        siteFinder.run()
        
        # mock input
        take_out_n = 1 # there is only copper in the structure
        supercell = siteFinder.atoms_supercell
        supercell_empty = siteFinder.atoms_supercell  # not needed for take_out_n = 1
        n_ion_supercell = len(supercell)

        # check the choose reflective path function here
        for unique_ion_pair in siteFinder.unique_ion_pairs:
            all_sym_paths = [pair for i, pair in enumerate(siteFinder.all_ion_pairs) 
                             if siteFinder.eq_to_ion_pairs[i] == unique_ion_pair]
            
            start_ind, final_ind = choose_reflective_path(
                all_sym_paths, take_out_n, 
                n_ion_supercell, supercell, supercell_empty)

        # assert only one unique path
        assert len(siteFinder.unique_ion_pairs) == 1
        # assert always the first one stored here
        assert siteFinder.unique_ion_pairs[0] == [0, 18]
        # assert length of all symmetry equiv paths
        assert len(all_sym_paths) == 6  # hexagonal symmetry
        # check that not the initial path, but the reflective one is returned
        assert [start_ind, final_ind] == [0, 35]
        
    def test_user_input_take_out_ion(self, tmp_path):
        """ Check that user input works and does what it is supposed to """
        from shutil import copy2
        current_mat_src = os.path.abspath('current_material.json')
        src_stability_out = os.path.abspath('./wf_files/iiwPBEU.check_stability.2498606.out')
        
        with cd(str(tmp_path.absolute())):
            copy2(current_mat_src, '.')
            copy2(src_stability_out, '.')
            task = TaskPreparePotnebTest(min_vac_spacing=4)
            task.run()
            with open("potneb_dictionary.json") as f:
                dic = json.load(f)
            # only one fodler, since only one charge state
            assert len(dic['pot_folders']) == 1


    
class TestCheckPotential:
    def create_bulk_cell(self, tmp_path):
        from ase.io import write
        from ase.build import molecule
        from ase.calculators.singlepoint import SinglePointCalculator
        # create needed directories
        d = tmp_path / "bulk"; d.mkdir()
        d = d / "FM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        
        # bulk cell
        with cd(str(d.absolute())):
            atoms = molecule('H2')
            newcalc = SinglePointCalculator(atoms, energy=-3)
            atoms.calc = newcalc
            write('test.traj', atoms)
            
    def create_pot_structure(self, tmp_path):
        from ase.io import write
        from ase.build import molecule
        from ase.calculators.singlepoint import SinglePointCalculator
        # create needed directories
        d = tmp_path / "potential"; d.mkdir()
        d = d / "1_4_0"; d.mkdir()  # just some mock indices
        d = d / "FM"; d.mkdir()
        d = d / "vasp_rx_PBESOL_U"; d.mkdir()
        
        with cd(str(d.absolute())):
            atoms = molecule('H2')
            newcalc = SinglePointCalculator(atoms, energy=-2)
            atoms.calc = newcalc
            write('test.traj', atoms)
    
    def create_ref_db(self, tmp_path):
        from ase.db import connect
        from ase.build import molecule
        from ase.calculators.singlepoint import SinglePointCalculator
        d = tmp_path / "starting_structures"; d.mkdir()
        with cd(str(d.absolute())):
            db = connect('references_ch.db')
            atoms = molecule('H2')
            newcalc = SinglePointCalculator(atoms, energy=-1.5)
            atoms.calc = newcalc
            db.write(atoms, elements='Mg')
        
    def test_full_check_task(self, tmp_path):
        """ test to see that everything works as expected """
        from iiwPBEU.check_potential import check_potential
        from shutil import copy2
        from pathlib import Path
        current_mat_src = os.path.abspath('current_material.json')
        
        with cd(str(tmp_path.absolute())):
            # create the ref db
            self.create_ref_db(tmp_path)
            mat_home_folder = get_wd([
                'calculations', 'Mg', 'check_potential_test'])
            with cd(mat_home_folder):
                # create needed folders and structures
                self.create_bulk_cell(Path('.'))
                self.create_pot_structure(Path('.'))
                copy2(current_mat_src, '.')
                pot = check_potential('test.traj')
                assert pot == -0.875
        
class TestCheckStability:
    def cerate_ref_db(self, tmp_path):
        from ase.db import connect
        from ase.build import fcc111
        from ase.calculators.singlepoint import SinglePointCalculator
        d = tmp_path / "starting_structures"; d.mkdir()
        with cd(str(d.absolute())):
            db = connect('references_ch.db')
            atoms = fcc111('Cu', [1, 1, 1], a=4)
            newcalc = SinglePointCalculator(atoms, energy=-2)
            atoms.calc = newcalc
            db.write(atoms)
            atoms = fcc111('Al', [1, 1, 1], a=4)
            newcalc = SinglePointCalculator(atoms, energy=-3)
            atoms.calc = newcalc
            db.write(atoms)
        
        # create empty, filled structures
        
            
            
            
            
            
            
        
        
        