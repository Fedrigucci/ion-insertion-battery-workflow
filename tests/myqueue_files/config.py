config = {
    'scheduler': 'slurm',
    'nodes': [
        ('xeon40', {'queue': 'slurm',
                    'cores': 40,
                    'memory': '350G'}),
        ('xeon24', {'queue': 'slurm',
                    'cores': 24,
                    'memory': '250G',
                    'mpiargs': '-mca pml cm -mca mtl psm2'}),
        ('xeon16', {'queue': 'slurm',
                    'cores': 16,
                    'memory': '60G'}),
        ('xeon8', {'queue': 'slurm',
                   'cores': 8,
                   'memory': '22G'}),
        ('xeon24_512', {'queue': 'slurm',
                        'cores': 24,
                        'memory': '500G',
                        'mpiargs': '-mca pml cm -mca mtl psm2'})
    ],
    'parallel_python': 'gpaw-python',
    'maximum_diskspace': 4
}
