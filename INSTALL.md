# How to install dependencies and set up the workflow
!!!This guide is for installing the workflow on niflheim!!!
For the installation we will use virtual environments to make sure the program 
versions are correct and do not interfere with other user installations.

### Set up the virtual environment
Before starting make sure to create the virtualenvironment on the "lowest ranking node". 
At the time of writing this is xeon8. The reason is that creating the virtualenvironment 
on a specific node is always forward compatible but not backward. That means if 
creating the virtenv on xeon24, only xeon40 nodes will be able to load the virtenv 
created with the python on xeon24. On xeon8 and xeon16 you will run into an error message.

First we need to make sure we are using a proper python version. We will not 
install python, but use the version that is already on niflheim. We will load the
python in the `.bashrc` file by adding these lines
```bash
# modules needed
module use /home/energy/modules/modules/all # VASP installation
module load Python/3.6.6-intel-2018b
```
Test that when typing in `python` it will actually open the python version just specified in the `.bashrc`.
Within your git repository folder we will now clone the workflow git repository using
```bash
$ git clone https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow.git
```
Now we will install the workflow using a virtenv. 
Start by choosing a name for the virtualenvironment, e.g.
```bash
$ virtualenv iiw_ase3.19
```
and add a line to your `.bashrc` for easy activation, e.g. something like

```bash
alias iiw_ase3.19="source /home/niflheim/$USER/iiw_ase3.19/bin/activate"

```

We will now install all the needed requirements. For this we will activate the virtenv by typing into the terminal
```bash
$ iiw_ase3.19
```
Within this virtual environment we will install the workflow package and its dependencies.
Run within the cloned git repository (i.e. the folder containig the `setup.py` file)
```bash
(iiw_ase3.19) $ pip install --editable .
```
We choose to install the package in development mode using the `--editbale` flag
(short `-e`),meaning that changes to the source directory will immediately affect
the installed package without needing to re-install.
We will allow tab-completion for myqueue by typing this command into the shell
```bash
(iiw_ase3.19) $ mq completion >> ~/.bashrc
```
Configuring the queue is done through the `config.py` file that should be added 
to you `$HOME/.myqueue/` folder. The best way is to copy this file from someone.

### Before running the workflow
After the installation requirements are satisfied we can set up the workflow. 

Consider running the tests before proceeding. 

You can listen to any branch in the git repository by setting `git branch --set-upstream-to=remotes/origin/vasp_PBE_U`.

Also consider setting up `set_cpu_arch` for being able to submit to multiple nodes.

Also make sure to load a clean version of VASP when on the node in your bashrc to get the correct executbale.

As a last step it is convenient to initialize a new myqueue folder for each 
inidvidual project. This allows you to move folders around without losing the 
paths afterwards, i.e. `mq ls` command will still list all your calculations.
Simply run
```bash
$ mq init
```
iniside your home calculation folder. This command will also copy the `config.py` 
file from whatever is in `$HOME/.myqueue/config.py`.
