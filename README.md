# Ion insertion workflow

![Pipeline](https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow/badges/master/pipeline.svg?job=python_tests)
![Coverage](https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow/badges/master/coverage.svg?job=python_tests)

Battery workflow using ASE, VASP and the workflow scheduler MyQueue.

The ion insertion workflow (iiwPBEU) intends to automate all steps for calculating bulk properties of insertion cathodes like stability (volume change and convex hull), open circuit voltages (high and low state of charge) as well as kinetic properties (migration barriers) using the Nudged Elastic Band method. All calculations are carried out in the framework of Density Functional Theory.


# Documentation

Everything about how to get started can be found in the [Documentation](https://asc-dtu.gitlab.io/workflows/ion-insertion-battery-workflow/).

Corresponding paper can be found here: http://dx.doi.org/10.1002/batt.201900152

Copyright (c) - see LICENSE



