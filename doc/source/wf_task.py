class WorkflowTask:
    def retrieve_information(self):
        """ Retrieve necessary information on calculation - e.g. which ion indice to take out? """
        
    def get_atoms_object(self):
        """ Get the atoms object used for relaxation - e.g. create defects"""
        
    def get_calculator_object(self):
        """ return the ASE calculator object used """
        
    def relax(self):
        """ Call the optimization including automatic error handling """
        
    def run(self):
        """ Run all the steps needed - below an example of how that could look """
        self.retrieve_information()
        self.get_atoms_object()
        self.get_calculator_object()
        self.relax()
    
if __name__=="__main__":
    WorkflowTask.run()

