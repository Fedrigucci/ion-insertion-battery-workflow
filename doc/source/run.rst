.. _run-the-workflow

Run the workflow
================

Setup a new project
###################
As a first step it is convenient to initialize a new myqueue folder for each inidvidual project. This allows you to move folders around without losing the paths afterwards, i.e. the ``mq ls`` command will still list all your calculations.
Simply create a home calculation folder (in this guide referred to as ``home_calc_folder``) and run iniside the command

::

    $ mq init

This command will also copy the ``config.py`` file from whatever is in ``$HOME/.myqueue/config.py``.

If not already present, copy the virtual environment folder into your ``home_calc_folder`` . The name of the folder has to be ``venv`` in order to be recognized by MyQueue.

Setup workflow and User input
#############################
The user input consists of the input *structure* as well as *input parameters* values .

For the starting structures, create a folder called ``starting_structures`` and inside that folder add your ASE database (SQLite) and name it ``materials.db`` containing the structures you want to calculate. Make sure that each row also contains a unique ``material_id`` key. This has to be chosen by the user and should ensure uniqueness of the calculations! 


.. warning ::
    Do not use illegal web characters (+ @) when defining the ``material_id`` since it will create a dedicated folder for the input provided. These forbidden characters include all of the following:
    ``~ " # % & * : < > ? / \ { | }.@``


In order to calculate the energy above the hull, you also need to provide a ``references_ch.db`` (ASE SQLite format) containing all the reference structures wanted. These reference structure calculations are not part of this workflow and have to be provided by the user. Depending on your calculation parameters, you might be able to directly download the structures from databases like the materialsproject.

The *input parameters* are parsed using a json-file called ``current_material.json``. This can look something like this

.. literalinclude:: ./../../tests/current_material.json
    :language: json



+---------------+----------------------------------------------------------------------------------------------+
| User input    | Description                                                                                  |
+===============+==============================================================================================+
| material_id   | Material ID matching wanted structure in `home_calc_folder/starting_structures/materials.db` |
+---------------+----------------------------------------------------------------------------------------------+
| ion           | Ion that will be intercalated or substituted in the structures (e.g. ion = "Mg")             |
+---------------+----------------------------------------------------------------------------------------------+
| magstate      | Magnetic ordering. Either non-magnetic ("NM") or high-spin ferro-magnetic ("FM")             |
+---------------+----------------------------------------------------------------------------------------------+
| cut_off_neb   | Bird-eye distance for searching for possible path given in Å                                 |
+---------------+----------------------------------------------------------------------------------------------+
| with_u_corr   | Apply a +U correction for the stability calculations or not (true or false)                  |
+---------------+----------------------------------------------------------------------------------------------+
| ch_energy     | Energy above the convex hull [eV/atom]                                                       |
+---------------+----------------------------------------------------------------------------------------------+
| potential     | Minimum Voltage of material [V]                                                              |
+---------------+----------------------------------------------------------------------------------------------+
| diffusivity   | kinetic barrier given in [eV]                                                                |
+---------------+----------------------------------------------------------------------------------------------+
| volume_change | In between charged/discharged unit cell [%]                                                  |
+---------------+----------------------------------------------------------------------------------------------+

**Optional User input**

Optional user input can be provided like

.. literalinclude:: ./current_material_optional.json
    :language: json

+-------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+
| Optional input    | Description                                                                                                                                         |
+===================+=====================================================================================================================================================+
| remove_n_ions     | Choose to remove n_ions from the structure at a time. Either "single_one" or "all_but_one". Choose "both_charge_states" to get both vacancy limits. |
+-------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+

Submit and monitor calculation
##############################

For interacting with the workflow, the scripts in ``ion-insertion-battery-workflow/workflow/`` should be copied to your home folder.
At this point, your folder structure should look something like this:

| home_calc_folder 
| ├── venv
| ├── starting_structures
| │	├── materials.db
| │	└── references_ch.db
| ├── current_material.json
| ├── collect.py
| ├── run_workflow_part_1.py
| ├── run_workflow_part_2.py
| ├── workflow_potneb.py
| └── workflow_start.py


All you need to now is to submit the workflow found in the repository `ion-insertion-battery-workflow/workflow/workflow_start.py`.


::

    $ mq workflow workflow_start.py


For running multiple calculations, it is more convenient to automate that process. You can find an example script for this in ``ion-insertion-battery-workflow/workflow/run_workflow_part_1.py``. Note that this example also contains some simple filter criteria that might be needed before submitting the calculation. Here, you just need to specify the user input again and then submit the workflow using


::

    $ python3 run_workflow_part_1.py


You can monitor the calculations using the ``mq ls`` comand. Once all calculations finished, you can run the second part of the workflow by supplying your material id in the ``ion-insertion-battery-workflow/workflow/run_workflow_part_2.py`` script and then call


::

    $ python3 run_workflow_part_2.py


.. note::
    The workflow is split in two parts. The reason is, that upon starting the workflow it is not known how many symmetry inequivalent elements are in the structure. Check how many symmetry inequivalent paths have been found before submitting the second part, since this involves heavy NEB calculations.

If everything went well, you can collect the data using ``ion-insertion-battery-workflow/workflow/collect.py`` . Simply call that script in your ``home_calc_folder``.

Resubmitting calculations
^^^^^^^^^^^^^^^^^^^^^^^^^

At one point a calculation will reach the wall-time limit. You can for instance check all the calculations that timed out using myqueue's ``mq ls -s T`` command. Simply remove that calculation from the myqueue list and resubmit following the approach described above.

Magical, right? In fact, an error-handler takes care of fixing common errors as well as resubmitting timed-out calculations. The idea is to parse the ``atoms`` and ``calculator`` object, scan the files of the current directory and search for error strings and then take action. Also have a look at the main class of the error handler for more details: :ref:`Error Handler` . Also (if you are curious) make sure to check the error handler implemented in `CUSTODIAN <https://materialsproject.github.io/custodian/_modules/custodian/vasp/handlers.html#VaspErrorHandler>`_ from which the error handler here takes all its inspiration!

The error handler here can in principle directly be applied in other projects, but you need to make sure it is really doing what you want it to do! This is also the reason, CUSTODIAN has not been chosen in the first place.

Adding a +U correction
######################

A hubbard U-correction can be added to the calculations as explained in the paper. For this to work, set the `with_u_corr=true` in the workflow input.

The values for the U-correction are then expected to be in the ``materials.db`` database for each specific row (material) in the `row.data` dictionary. As an example, adding a +U correction on the Fe d-orbitals in a MgFeO structure works through specifying the `row.data` entry as follows:

.. literalinclude:: ./u_corr.json
    :language: json

The convetion follows standard VASP conventions. If you want to make sure everything is set up as expected, have a look at the function that converts the input: ``iiwPBEU.battery_tools.get_starting_structure_from_db()`` .

