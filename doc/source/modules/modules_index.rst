API reference
==================

.. toctree::
    :maxdepth: 1

    ./tasks_relax
    ./tasks_neb
    ./error_handler.rst
