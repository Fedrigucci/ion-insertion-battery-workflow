"""
Created on Tue Apr 19 2021

Relax NEB paths using CIC - NEB

@author: Felix Bölle <feltbo@dtu.dk>
"""
from iiwPBEU.tasks_neb import TaskCICNEB


def relax_cicneb():
    task = TaskCICNEB()  # barrier in eV
    task.run()
    return


if __name__ == "__main__":
    relax_cicneb()
