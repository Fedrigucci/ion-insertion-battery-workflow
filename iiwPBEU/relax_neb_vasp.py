"""
Created on Tue Apr 30 2019

Logic for initial NEB path relaxation

@author: Felix Bölle <feltbo@dtu.dk>
"""
import json

from iiwPBEU.tasks_neb import TaskRMINEB, TaskCNEB


def relax_neb():
    """ If symmetry - run RMINEB, else run traditional CNEB """
    with open('symmetry.json') as f:
        dic = json.load(f)

    if dic['symmetric'] == 1:
        # that means it is reflective, start RMI-NEB
        task = TaskRMINEB()
        task.run()
    else:
        task = TaskCNEB()
        task.run()

    return


if __name__ == "__main__":
    relax_neb()
