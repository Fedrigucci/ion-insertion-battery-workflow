# ==============================================================================
#                   RELAX BULK WITHOUT ION IN STRUCTURES
# ==============================================================================
import iiwPBEU.vasp_setups as vs
from iiwPBEU.battery_tools import get_starting_structure_from_db
from iiwPBEU.input import UserInput
from iiwPBEU.tasks_relax import TaskRelax


class TaskRelaxBulkEmpty(TaskRelax):
    """ Relax a bulk structure """
    _calc_hf_nback = 6  # how far is calculations home folder back
    _mat_hf_nback = 3  # material home folder

    def __init__(self, task_name='relax_bulk_empty', *args, **kwargs):
        super(TaskRelaxBulkEmpty, self).__init__(*args, **kwargs)
        self.task_name = task_name

    def run(self):
        """ run all steps """
        self.retrieve_information()
        atoms, hubbard, U_dic = self.get_atoms_object()
        calc = self.get_calculator_object(atoms, hubbard, U_dic)
        status = self.relax(atoms, calc, self.magstate)
        return status

    def retrieve_information(self):
        user_args = UserInput.read_user_input(self._mat_hf_nback)
        self.material_id = user_args["material_id"]
        self.magstate = user_args["magstate"]
        self.ion = user_args["ion"]
        self.with_u_corr = user_args["with_u_corr"]

    def get_atoms_object(self):
        atoms, hubbard, U_dic = get_starting_structure_from_db(
            gobacknfolders=self._calc_hf_nback,
            material_id=self.material_id,
            with_u_corr=self.with_u_corr)

        # take out ALL ion atoms from unit cell
        ion_indices = [atom.index for atom in atoms if atom.symbol == self.ion]
        del atoms[ion_indices]

        return atoms, hubbard, U_dic

    def get_calculator_object(self, atoms, hubbard, U_dic):
        calc = vs.get_vasp_calc_initial_relaxation_bulk(atoms, hubbard, U_dic)
        return calc

    def relax(self, atoms, calc, magstate):
        status = vs.initial_relaxation_routine_vasp_bulk_full_convergence(
            atoms, calc, magstate)  # might set istart = 1 tag for calculator
        return status


def relax_bulk_empty():
    task_bulk = TaskRelaxBulkEmpty()
    status = task_bulk.run()
    return status


if __name__ == "__main__":
    status = relax_bulk_empty()
