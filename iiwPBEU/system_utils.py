#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Mostly for creating and handling the folder structure of the workflow

@author: Felix Bölle <feltbo@dtu.dk>
'''

from contextlib import contextmanager
import os


@contextmanager
def cd(newdir):
    """ Change/Create working directory and catch errors """
    prevdir = os.getcwd()
    try:
        os.mkdir(newdir)
    except OSError:
        pass
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


@contextmanager
def simple_cd(newdir):
    """ Change working directory and catch errors """
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def get_wd(wanted_folders):
    """ Function creating folder structures given a list of strings

    Parameters
    ---------
    wanted_folders : list of strings
        List with strings defining the wanted folders. It will create all
        folders given the order of the list, even if they do not extist.
        The folders will be created in the current working directory.

    Returns
    ---------
    path_to_last_folder : string
        Returns absolute path to last folder in the folder structure. That
        means a path to the last entry in the wanted_folders list.

    See Also
    ---------
    cd : function from nanotube_tools, creates folders and catches any
         errors or exceptions

    Examples
    ---------
    >>> folders = ['1','2','3']
    >>> path_to_folder = get_wd(wanted_folders)
    >>> print(path_to_folder)
    'home/1/2/3'

    """
    cwd = os.getcwd()

    for folder in wanted_folders:
        with cd(os.path.join(cwd, folder)):
            cwd = os.getcwd()
            continue

    return cwd


def get_all_paths_f(filename, cwd='.'):
    """ returns paths to all files in cwd and all subdirectories """
    paths_to_f = []
    for path, dirs, files in os.walk(cwd):
        if filename in files:
            paths_to_f.append('/'.join((path, filename)))

    return paths_to_f


def delete_files_cwd(filename, cwd='.'):
    """ will delete all files in cwd and all subdirectories """
    paths_to_f = get_all_paths_f(filename, cwd=cwd)

    if paths_to_f:
        for path_to_f in paths_to_f:
            os.remove(path_to_f)
    else:
        print("No files found to delete")


def touch(path):
    """ Resemble the touch terminal command behavior """
    with open(path, 'a'):
        os.utime(path, None)
