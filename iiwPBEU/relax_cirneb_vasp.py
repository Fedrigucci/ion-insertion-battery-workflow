"""
Created on Tue Apr 30 2019

Relax NEB paths using CI-RNEB

@author: Felix Bölle <feltbo@dtu.dk>
"""
from iiwPBEU.tasks_neb import TaskCIRNEB
from iiwPBEU.battery_tools import get_current_material_dic


def get_barrier_threshold(gobacknfolders):
    dic = get_current_material_dic(gobacknfolders)
    return dic["thresholds"]["diffusivity"]


def relax_cirneb():
    bar = get_barrier_threshold(gobacknfolders=TaskCIRNEB._hf_material_nback)
    task = TaskCIRNEB(barrier_threshold=bar)  # barrier in eV
    task.run()
    return


if __name__ == "__main__":
    relax_cirneb()
