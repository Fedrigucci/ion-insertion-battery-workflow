#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 13:32:12 2019

Script for storing all VASP relaxation inputs and relaxation schemes

@author: Felix Bölle <feltbo@dtu.dk>
'''
import os
import shutil
import numpy as np
import subprocess


from ase.calculators.vasp import Vasp2
from ase.io import read

from iiwPBEU.vasp_helper import get_number_ionic_steps_vasp
from iiwPBEU.fix_dft_calculation import FixVaspCalculation, FixVaspError


def get_custom_kpts(cell, density_criterion=30):
    """ Creates kpoint grids with user specified density

    Parameters
    ----------
    cell : ndarray
        Cell dimensions of the input structure. In ASE this would equal the
        function call cell = ase_atoms.get_cell()

    density_criterion : int
        The density criterion defines how many kpoints will be chosen along
        the axis. The amount of kpoints is calculated as
        n_kpoints = int(density_criterion/a) + 1 , where a is the lenght of the
        lattice vector in either of one of the cell directions. The + 1 ensures
        for large cells, that there is at least 1 kpoint in each direction

    Returns
    -------
    kpts : narray
        Numpy array with number of kpoints in x,y and z direction

    Examples
    ---------
    >>> cell = np.array([[5,0,0],[0,5,0],[0,0,5]])
    >>> kpts = get_custom_kpts(cell, density_criterion = 30)
    >>> print(kpts)
    [7,7,7]

    """

    t1, t2, t3 = cell[0, :], cell[1, :], cell[2, :]

    kpts = [int(density_criterion/np.linalg.norm(t1)) + 1,
            int(density_criterion/np.linalg.norm(t2)) + 1,
            int(density_criterion/np.linalg.norm(t3)) + 1]

    return np.array(kpts)


def setup_magnetic_moments_vasp(atoms, calc, magstate):
    ''' Sets up initial magnetic moments

    Raises
    ---------
    NotImplementedError
    Raised if magnetic state is not one of 'NM' (non - magnetic) or 'FM'
    (ferro - magnetic).
    '''

    if magstate == 'NM':
        pass
    elif magstate == 'FM':
        # set initial magnetic moment to always find high spin states
        dic_magmoms = get_magnetic_moment_dic()
        magmoms = []
        for symbol in atoms.get_chemical_symbols():
            if symbol in list(dic_magmoms.keys()):
                magmoms.append(dic_magmoms[symbol]['0'])  # magnetic elements
            else:
                magmoms.append(1)  # not completely 0
        magmoms = np.array(magmoms)
        atoms.set_initial_magnetic_moments(magmoms)
        calc.set(ispin=2)  # make calculation spin polarized
        calc.set(lorbit=11)  # write magnetic data to OUTCAR
    else:
        msg = "Magnetic state {} not implemented".format(magstate)
        raise NotImplementedError(msg)

    return atoms, calc


def get_functional():
    """ sets default functional also used to build folder structure """
    functional_choice = 'PBESOL'
    return functional_choice


def get_atoms_filename():
    """ atoms filename used in all the scripts """
    atoms_filename = 'OUTCAR'
    return atoms_filename


def get_ncore():
    """ default ncore value """
    ncore = 4
    return ncore


def get_vasp_calc_default(atoms):
    ''' Default VASP Calculator '''

    functional = get_functional()
    ncore = get_ncore()
    kpts = get_custom_kpts(atoms.get_cell(), density_criterion=30)

    calc_vasp = Vasp2(xc=functional,
                      encut=520,  # max cut off for O pseudo potential=1.3*400
                      prec='accurate',  # default, does not overwrite ENCUT
                      ismear=-5,
                      sigma=0.05,
                      isif=3,  # change cell shape
                      ncore=ncore,
                      algo='Fast',
                      kpts=kpts,
                      nsw=250,
                      nelmin=8,
                      ediff=0.00005,
                      ediffg=-0.03,  # juanmas suggestion
                      ibrion=2,
                      gamma=True,
                      lwave=True,
                      lcharg=False,
                      setups='materialsproject')

    return calc_vasp


def get_vasp_calc_initial_relaxation_bulk(atoms, hubbard=False, U_dic={}):
    ''' VASP Calculator for initial Mg-bulk relaxations

    Parameters
    -----------
    hubbard : boolean
    Aplly +U corrections depending on if it was applied in materialsproject

    U_dic : dictionary
    Plus U correction values in ASE format

    '''

    calc_vasp = get_vasp_calc_default(atoms)
    if hubbard:
        calc_vasp.set(ldau=True,
                      ldau_luj=U_dic)

    return calc_vasp


def get_vasp_calc_potential_relaxation(atoms,
                                       ncore=4,
                                       hubbard=False,
                                       U_dic={}):
    ''' VASP Calculator for high ion concentration potential calculations

    Notes
    ------
    Isif 0 keeps the cell shape. This allows reflective NEB to work since
    symmetry is kept
    '''

    calc_vasp = get_vasp_calc_default(atoms)
    calc_vasp.set(isif=0)
    # how many cores
    ncore = get_ncore()
    calc_vasp.set(ncore=ncore)
    if hubbard:
        calc_vasp.set(ldau=True,
                      ldau_luj=U_dic)

    return calc_vasp


def get_vasp_calc_NEB(atoms, ncore=4):
    ''' VASP Calculator for Mg potential calculations '''

    ncore = get_ncore()
    calc_vasp = get_vasp_calc_default(atoms)
    calc_vasp.set(isif=0)
    calc_vasp.set(nsw=0)
    calc_vasp.set(ibrion=-1)
    calc_vasp.set(ncore=ncore)
    calc_vasp.set(algo='all')
    calc_vasp.set(isym=1)
    calc_vasp.set(symprec=1e-8)
    calc_vasp.set(ediffg=-0.05)
    return calc_vasp


def relaxation_routine_vasp_bulk(atoms, calc, magstate,
                                 delete_files=True, max_restart=3):
    """ automatically restart a calculation  if any error found

    Parameters
    ------------
    max_restart : int
        Number of times the FixVaspApplication should try to fix the error
    """
    n_restarts = 0
    for n_restarts in range(max_restart):
        try:
            state = _relaxation_routine_vasp_bulk(
                atoms, calc, magstate, delete_files=delete_files)
            return state
        except FixVaspError as e:
            raise FixVaspError(e.message)
        except MyVaspError as e:
            raise MyVaspError(e.message)
        except BaseException as e:
            print("Something went wrong with msg: " + str(e))
            n_restarts += 1

    msg = ("Maximum number of routine restarts reached"
           f" {n_restarts}/{max_restart}")
    raise ValueError(msg)


def _relaxation_routine_vasp_bulk(atoms, calc, magstate, delete_files=True):
    """ Relax all vasp relaxations using isif 3 in a consistent way

    Parameters
    -----------
    atoms : ASE-atoms object

    calc : ASE-calculator object (VASP)

    magstate : string
    Magnetic state of system.

    delete_files : boolean
    If set to True will clean up directory to save diskspace (WAVECAR)

    """
    # check for errors to adjust calculator/atoms object
    fixvasp = FixVaspCalculation(atoms=atoms,
                                 calculator=calc)
    atoms, calc = fixvasp.fix_if_needed()

    # set magnetic moments
    atoms, calc = setup_magnetic_moments_vasp(atoms, calc, magstate)

    # get potential energy
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    shutil.copy('OUTCAR', 'OUTCAR_0')
    nsteps = get_number_ionic_steps_vasp('OUTCAR')

    # ---------------------- ERROR HANDLING -----------------------------------
    errors_to_check = ['forces', 'nsteps']
    status = check_relaxation_errors(atoms, calc, errors_to_check, nsteps)
    # First, check that configuration is a tube. If not, crash the calculation
    if status in errors_to_check:
        return status
    # -------------------------------------------------------------------------

    # remove WAVECAR file after finishing to save disk space
    if delete_files:
        os.remove('WAVECAR')

    return 'finished'


def initial_relaxation_routine_vasp_bulk_full_convergence(
        atoms, calc, magstate, max_restart=3):
    """ automatically restart a calculation  if any error found

    Parameters
    ------------
    max_restart : int
        Number of times the FixVaspApplication should try to fix the error
    """
    n_restarts = 0
    for n_restarts in range(max_restart):
        try:
            state = _initial_relaxation_routine_vasp_bulk_full_convergence(
                atoms, calc, magstate)
            return state
        except FixVaspError as e:
            raise FixVaspError(e.message)
        except MyVaspError as e:
            raise MyVaspError(e.message)
        except BaseException as e:
            print("Something went wrong with msg: " + str(e))
            n_restarts += 1

    msg = ("Maximum number of routine restarts reached"
           f" {n_restarts}/{max_restart}")
    raise ValueError(msg)


def _initial_relaxation_routine_vasp_bulk_full_convergence(atoms,
                                                           calc,
                                                           magstate):
    """ Relax all vasp relaxations using isif 3 in a consistent way

    Notes
    ------
    This routine relaxes a structure until it converges in a single point
    calculation. For convergence it is important to save the WAVECAR. Otherwise
    local structural minima due to random wavefunction initialization in VASP
    can occur (that comes from experience when relaxing Nanotubes and 2D sheets
    containing vacuum).
    More info: https://www.vasp.at/vasp-workshop/slides/accuracy.pdf

    """
    # check for errors to adjust calculator/atoms object
    fixvasp = FixVaspCalculation(atoms=atoms,
                                 calculator=calc)
    atoms, calc = fixvasp.fix_if_needed()

    # set magnetic moments
    atoms, calc = setup_magnetic_moments_vasp(atoms, calc, magstate)

    # get potential energy
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    shutil.copy('OUTCAR', 'OUTCAR_0')
    nsteps = get_number_ionic_steps_vasp('OUTCAR')

    # ---------------------- ERROR HANDLING ------------------------------------
    errors_to_check = ['forces', 'nsteps']
    status = check_relaxation_errors(atoms, calc, errors_to_check, nsteps)
    # First, check that configuration is a tube. If not, crash the calculation
    if status in errors_to_check:
        return status

    # --------------------------------------------------------------------------

    # now rerelax the structure due to change of cell shape until it converges
    # within a single relaxation step
    counter = 1
    # set explicitly, but will also be set automatically since WAVECAR exists
    calc.set(istart=1)
    while nsteps > 1:
        # read in new cell structure and atom position before new relaxation
        atoms = read('OUTCAR')
        # new plane waves adjusted to new cell shape - based on existing WAVECAR
        atoms.set_calculator(calc)
        atoms.get_potential_energy()
        shutil.copy('OUTCAR', 'OUTCAR_{}'.format(counter))
        nsteps = get_number_ionic_steps_vasp('OUTCAR')
        counter += 1

        # -------------------ERROR HANDLING ------------------------------------
        errors_to_check = ['nsteps', 'forces', 'counter']
        status = check_relaxation_errors(atoms, calc, errors_to_check,
                                         nsteps, counter=counter)

        if status in errors_to_check:
            return status

        # ----------------------------------------------------------------------

    # remove WAVECAR file after finishing to save disk space
    os.remove('WAVECAR')

    return 'finished'


def check_relaxation_errors(atoms, calc, errors, nsteps, counter=0):
    ''' Checks for relaxation errors and returns error message if any '''
    for error in errors:
        if error == 'nsteps':
            if nsteps == calc.todict()['nsw']:
                msg = f"Maximum number of ionic steps reached ({nsteps})"
                raise MyVaspError(msg)
        if error == 'forces':
            fmax = atoms.get_forces().max()
            if fmax > abs(calc.todict()['ediffg']):
                msg = "Forces not converged"
                raise ValueError(msg)
        if error == 'counter':
            if counter == 10:
                msg = f"Maximum number of OUTCAR restarts reached ({counter})"
                raise MyVaspError(msg)

    return 'no error'


def get_magnetic_moment_dic():
    """ Dictionary that contains values for initializing spin states
    Some values taken from
    https://github.com/materialsproject/pymatgen/blob/master/pymatgen/io/vasp/VASPIncarBase.yaml
    """
    magmoms = {'Ce': {'0': 5, '3+': 1},
               'Co': {'0': 5, '3+': 0.6, '4+': 1},
               'Cr': {'0': 5},
               'Dy': {'3+': 5},
               'Er': {'3+': 3},
               'Eu': {'0': 10, '2+': 7, '3+': 6},
               'Fe': {'0': 5},
               'Gd': {'3+': 7},
               'Ho': {'3+': 4},
               'La': {'3+': 0.6},
               'Lu': {'3+': 0.6},
               'Mn': {'0': 5, '3+': 4, '4+': 3},
               'Mo': {'0': 5},
               'Nd': {'3+': 3},
               'Ni': {'0': 5},
               'Pm': {'3+': 4},
               'Pr': {'3+': 2},
               'Sm': {'3+': 5},
               'Tb': {'3+': 6},
               'Tm': {'3+': 2},
               'V': {'0': 5},
               'W': {'0': 5},
               'Yb': {'3+': 1},
               'Ti': {'0': 5},  # 3 unpaired electrons max
               'Cu': {'0': 3}
               }

    return magmoms


def get_magnetization_OUTCAR(path_to_OUTCAR='.'):
    """ returns the total magnetic moment from the OUTCAR file

    Returns
    ---------
    out : int
    Integer closest to total magnetization found in OUTCAR file
    """

    cmd = (f"grep -rnw 'magnetization' {path_to_OUTCAR} |"
           " grep number |"
           " tail -n 1 |"
           " awk '{print $NF}'")

    out = subprocess.check_output(cmd, shell=True)
    out = str(out)
    out = out.split("'")[1].split('\\')[0]
    out = float(out)
    out = int(round(out))  # round to closest integer

    return out

# Exceptions


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class MyVaspError(Error):
    """Exception raised for errors user defined Vasp errors

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message='MyVasp Error was raised'):
        self.message = message
