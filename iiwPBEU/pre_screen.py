"""
Created on Mon Dec  2, 2019

Pre screen functions for discarding structures before running foll workflow

@author: felix
"""
import re
import numpy as np

from ase.data import atomic_numbers, covalent_radii

import iiwPBEU.data as iiw_data
from iiwPBEU.percolating_path import check_element_trapped,\
                                     create_sphere_from_neighbor_list


def check_atom_percolating(atoms, cutoff, el_ind, tol=1,
                           visualize=False):
    """
    Checks if atom has a percolating path
    """

    percolating, vec = check_element_trapped(
        atoms,
        max_path_length=cutoff,
        el_ind=el_ind,
        tol=tol,
        visualize=visualize)

    return percolating


def check_unwanted_elements(atoms):
    """ Any unwanted elements in structure """
    chem_syms = atoms.get_chemical_symbols()

    radioactive_els = ['Th', 'U']
    radioactive = set(chem_syms).intersection(set(radioactive_els))
    if radioactive:
        return ('radioactive', radioactive)

    toxic_els = ['Hg']
    toxic = set(chem_syms).intersection(set(toxic_els))
    if toxic:
        return ('toxic', toxic)

    return False


def count_metal_oxidation_state(formula, ion=None):
    """ Calculates oxidation state of metal in structure

    Parameters
    --------------
    formula :  str
    Some sort of string containing amount of each species in the structure
    Could be something like formula = 'Mg2Mo6S8' (magnificent chevrel)

    ion : str
    Ion in structure that will be removed.

    Returns
    --------------
    ox_state_metal : float
    Current oxidation state of metal in structure

    max_ox_state_metal : int
    Maximum oxidation state that metal can have

    frac_ions_rm : float
    Fraction of how much of the ions can be taken out of the structure until
    maximum oxidation state of metal is reached. 1 means all ions can be removed
    without starting oxidizing other species (in theory) and 0 means the metal
    is already in its max oxidation state and no ions can be taken out without
    oxidizing other species. Take frac_ions_rm*100 to get precentage.

    metal : str
        The metal that is oxidized, simply the one with the highest oxidation
        state is chosen

    """
    max_ox_state_metals = iiw_data.max_ox_state_metals
    non_metal_ox_states = iiw_data.non_metal_ox_states

    mat_el = re.findall('[A-Z][^A-Z]*', formula)

    # separate amount element from element string
    mat_dic = {}
    for el in mat_el:
        pure = []
        for char in el:
            if not char.isdigit():
                pure.append(char)
        if any([i for i in el if i.isdigit()]):
            n_el = int(''.join([i for i in el if i.isdigit()]))
        else:
            n_el = 1
        only_el = ''.join(pure)
        mat_dic[only_el] = n_el

    non_metal = [e for e in list(mat_dic.keys())
                 if e not in list(max_ox_state_metals.keys())]
    metals = [e for e in list(mat_dic.keys()) if e in list(
        max_ox_state_metals.keys())]
    if len(metals) > 1:
        max_v = 0
        for m in metals:
            value = max_ox_state_metals[m]
            if value > max_v:
                max_v = value
                metal = m
        to_add_non = [e for e in metals if e != metal]
        non_metal += to_add_non
        for entry in to_add_non:
            non_metal_ox_states[entry] = max_ox_state_metals[entry]
    else:
        metal = metals[0]

    # now calculate oxidation state of metal
    ox_state = 0
    for el in non_metal:
        ox_state += mat_dic[el] * non_metal_ox_states[el]

    # oxidation state of metal
    ox_state_metal = (ox_state/mat_dic[metal])*(-1)
    max_ox_state_metal = max_ox_state_metals[metal]

    if ion:
        # how much of the ion are removable
        # if not exceeding max mateal oxstate ?
        frac_ions_rm = ((max_ox_state_metal - ox_state_metal) * mat_dic[metal])\
            / (mat_dic[ion] * non_metal_ox_states[ion])
        frac_ions_rm = max(0, min(1, frac_ions_rm))
        return ox_state_metal, max_ox_state_metal, frac_ions_rm
    else:
        return ox_state_metal, max_ox_state_metal, metal


def get_specific_neighbors_voronoi(atoms, element, neighbor_els):
    """
    Get only neighbors of specific kind using Voronoi Tesselation
    and covalent Filter

    Parameters
    ------------
    atoms : ase-atoms object
    element : str
        For this element each atom coordination is checked.
    neighbor_els : list of str
        Neighboring elements, e.g. only anions considered for covalent
        bond filter
    """
    inds = [atom.index for atom in atoms if atom.symbol == element]
    amount_neighbors = []
    for ind in inds:
        _, bond_list = get_el_coordination_voronoi(atoms,
                                                   ind=ind,
                                                   cutoff=10,
                                                   verbose=False)
        n_neighbors, bond_list = neighbors_filter_covalent(bond_list,
                                                           verbose=False)
        # only include neighbors that are anions
        bond_list_neighbor_els = []
        for pair, d in bond_list:
            if any([True for el in pair.split('-') if el in neighbor_els]):
                bond_list_neighbor_els.append((pair, d))

        amount_neighbors.append(len(bond_list_neighbor_els))

    return np.array(amount_neighbors)


def get_el_coordination_voronoi(atoms, ind, cutoff=10,
                                visualize=False, verbose=False):
    """
    Returns the coordination number of a specific element

    Parameters
    ----------
    atoms : ase-atoms object
    ind : int
        Index of element of interest.
    cutoff : float
        cutoff value for creating neighbor list
    visualize : Boolean
        Show environment of atom with ase-gui
    verbose : Boolean

    Returns
    -------
    n_neighbors : int
        How many neighbors after voronoi tesselation
    bond_list : list of tuples
        Info on which neighbors does atom have and what is the bond distance to
        these neighbors. E.g. bond_list = [('Mg-O', 2.561)] means a single
        neighbor of Mg with O and they are 2.561 apart

    """
    from scipy.spatial import Voronoi
    from ase.neighborlist import neighbor_list
    import numpy as np

    i, j, d, D = neighbor_list('ijdD', atoms, cutoff=cutoff)

    if visualize:
        visualize_from_neighbor_list(atoms, ind, i, j, D)

    # Create the environment of atoms[ind]
    syms = atoms.get_chemical_symbols()
    mask = i == ind
    points = [[0, 0, 0]]  # place element of interest on origin
    for vec in D[mask]:
        points.append(vec)

    # VORONOI part
    symbols_nl = [syms[ind]]  # the neighbor list symbols in environment
    symbols_nl += [syms[i] for i in j[mask]]

    vor = Voronoi(points)
    rp1 = 0  # ridge point 0 is the origin or atom of interest
    n_neighbors = 0
    bond_list = []
    for ridge_points, ridge_vertices in vor.ridge_dict.items():
        if rp1 in ridge_points:

            if -1 in ridge_points:
                raise ValueError("Infinite Voronoi region, increase cut-off")

            rp2 = [v for v in ridge_points if v != rp1][0]
            sym1, sym2 = symbols_nl[rp1], symbols_nl[rp2]
            # bondlength
            bl = np.linalg.norm(d[mask][rp2 - 1])

            bond_list.append((f'{sym1}-{sym2}', bl))
            n_neighbors += 1
            if verbose:
                print("Found neighbor %d %s-%s with bondlength %5.3f" % (
                    n_neighbors, sym1, sym2, bl))

    return n_neighbors, bond_list


def neighbors_filter_covalent(bond_list, tol=1.2, verbose=False):
    """
    Only consider neighbor pairs that are covalently bonded

    Parameters
    ----------
    bond_list : list of tuples
        Comes from get_el_coordination_voronoi()
    tol : float
        Tolerance parameter on how much is considered a covalent bond. If set
        to 1 it means tolerated is everything that is equal to the covalent
        bond length of the two elements. The covalent bond length is calculated
        as adding the covalent radius of each element together.
        Default is 1.2, that is 20% on top.

    Returns
    -------
    n_neighbors : int
        How many neighbors after covlant filter
    bond_list_new : list of tuples
        Info on which neighbors does atom have and what is the bond distance to
        these neighbors. E.g. bond_list_new = [('Mg-O', 2.561)] means a single
        neighbor of Mg with O and they are 2.561 apart

    """

    bond_list_new = []
    for bond_pair, bl in bond_list:
        sym1, sym2 = bond_pair.split('-')
        cov_r1 = covalent_radii[atomic_numbers[sym1]]
        cov_r2 = covalent_radii[atomic_numbers[sym2]]
        cov_av = (cov_r1 + cov_r2) * tol

        if verbose:
            print("Bond-pair %s-%s with bondlength %5.3f, acceptable %5.3f" % (
                sym1, sym2, bl, cov_av))

        if bl < cov_av:
            bond_list_new.append((bond_pair, bl))

    n_neighbors = len(bond_list_new)
    return n_neighbors, bond_list_new


def visualize_from_neighbor_list(atoms, ind, i, j, D):
    """
    Visualize environment after applying neighbor_list

    Parameters
    ----------
    atoms : ase-atoms object
    ind : int
        Index of interest
    i,j,D : ase-neighbor_list output
        Output from ase.neighborlist.neighbor_list

    """
    from ase.visualize import view
    atoms = create_sphere_from_neighbor_list(atoms, ind, i, j, D)

    view(atoms)
