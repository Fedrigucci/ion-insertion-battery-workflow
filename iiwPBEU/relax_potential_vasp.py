# ==============================================================================
#                   RELAX POTENTIAL STRUCTURES
# ==============================================================================
import os
import json
import glob
import shutil

from iiwPBEU.battery_tools import get_supercell_from_folder,\
    create_unique_defect, \
    get_relaxed_empty_from_folder,\
    get_starting_structure_from_db
from iiwPBEU.input import UserInput
import iiwPBEU.vasp_setups as vs
from iiwPBEU.tasks_relax import TaskRelax


class TaskRelaxBulkPotential(TaskRelax):
    """ Relax a bulk structure with defects in high/low SOC """
    _calc_hf_nback = 7  # how far is calculations home folder back
    _mat_hf_nback = 4  # how far back is material home folder

    def __init__(self, task_name='relax_bulk_potential', *args, **kwargs):
        super(TaskRelaxBulkPotential, self).__init__(*args, **kwargs)
        self.task_name = task_name

    def run(self):
        """ run all steps """
        self.retrieve_information()
        atoms, hubbard, U_dic = self.get_atoms_object()
        calc = self.get_calculator_object(
            atoms, hubbard, U_dic, self.calc_already_done)
        status = self.relax(atoms, calc, self.magstate)
        return status

    def retrieve_information(self):
        user_args = UserInput.read_user_input(self._mat_hf_nback)
        self.material_id = user_args["material_id"]
        self.magstate = user_args["magstate"]
        self.with_u_corr = user_args["with_u_corr"]

        folder_info = os.getcwd().split('/')
        # funcitonal specifications --> e.g. +U or not
        self.func_spec = folder_info[-1]
        ion_out, ion_max, ion_ind = folder_info[-3].split('_')
        self.calc_already_done = False  # by default this is a new calculation
        # convert to ints
        self.ion_out = int(ion_out)
        self.ion_max = int(ion_max)
        self.ion_ind = int(ion_ind)

    def get_atoms_object(self):
        # get atoms object
        supercell = get_supercell_from_folder(self._mat_hf_nback)
        # get information on +U correction if needed
        _, hubbard, U_dic = get_starting_structure_from_db(
            gobacknfolders=self._calc_hf_nback,
            material_id=self.material_id,
            with_u_corr=self.with_u_corr)

        if self.ion_out == 1:
            atoms = create_unique_defect(supercell,
                                         ion_ind=self.ion_ind,
                                         ion_out=self.ion_out,
                                         ion_max=self.ion_max)
        elif self.ion_out + 1 == self.ion_max:
            # get empty supercell structure from empty relaxed
            uc_empty = get_relaxed_empty_from_folder(
                self._mat_hf_nback, self.magstate)
            # get information from inequivalent site finder
            potneb_dic = '.' + '/..'*self._mat_hf_nback\
                         + '/potneb_dictionary.json'
            with open(potneb_dic) as f:
                dic = json.load(f)
            rep = dic['repeat_unitcell']
            supercell_empty = uc_empty.repeat(rep)

            # create defect structure by inserting ion into empty supercell
            atoms = create_unique_defect(supercell_empty,
                                         ion_ind=self.ion_ind,
                                         ion_out=self.ion_out,
                                         ion_max=self.ion_max,
                                         supercell=supercell)

        return atoms, hubbard, U_dic

    def get_calculator_object(self, atoms, hubbard, U_dic, calc_already_done):
        if 'U' in self.func_spec:
            calc = vs.get_vasp_calc_potential_relaxation(
                atoms, hubbard=hubbard, U_dic=U_dic)
        else:
            calc = vs.get_vasp_calc_potential_relaxation(atoms)
            # now check if previous calculation was also carried out without
            # +U correction
            if hubbard is False:
                # then the vasp_rx_PBE_U was carried out without
                # applying +U corrections
                calc_already_done = True
        self.calc_already_done = calc_already_done

        return calc

    def relax(self, atoms, calc, magstate):
        # If previous calculation has been carried out without +U as well,
        # simply copy folders
        functional = vs.get_functional()
        if 'U' not in self.func_spec and self.calc_already_done:
            fs = glob.glob(os.path.join(f'./../vasp_rx_{functional}_U/', '*'))
            for filename in fs:
                shutil.copy(filename, '.')
            status = 'finished'
        else:
            status = vs.relaxation_routine_vasp_bulk(
                atoms, calc, magstate,
                delete_files=False)  # keep WAVECAR for NEB

        return status


def relax_potential():
    task_bulk = TaskRelaxBulkPotential()
    status = task_bulk.run()
    return status


if __name__ == "__main__":
    status = relax_potential()
