"""
Script for checking if path is percolating or not
"""
import json
import copy
import numpy as np

from ase.visualize import view
from ase.neighborlist import neighbor_list
from ase import Atoms

from iiwPBEU.neb.neb_path import NEBPath


def check_element_trapped(atoms, max_path_length, el_ind,
                          tol=1, visualize=False, verbose=False):
    """ Checks if given atom has paths that are percolating in cell

    Parameters
    -----------
    atoms : ase-atoms object
        The structure of interest. Should be unit cell for computational sake.
    max_path_length : float
        The maximum diffusion path length allowed by the user (bird-distance).
    el_ind :  int
        Index of the element that needs to be tested for percolation paths.
    tol : float
        Gives tolerance for marking a path percolating. If atoms get closer
        than tol value to neighboring atoms in path, path is not considered
        percolating. Choose wisely. Given in Ansgtrom.
    visualize : Boolean
        show atoms_sphere and final path with only elements in the path.

    Returns
    --------
    percolating : boolean
        True if element has a diffusion path according to given constraints
    macro_vector : array
        Vector pointing from initial position to final position. That is
        the diffusion direction

    Notes
    ------
    That function should work like this: Accept an ion indice as input from
    any given structure. Based on this atom it will find a precolating
    path if possible and returns the corresponding macroscopic diffusion
    vector. If there is no percolating path possible from the starting ion,
    it will let you know.

    """

    longest_diagonal_cell = get_longest_diagonal_cell(atoms.cell)
    cutoff_serach_sphere = longest_diagonal_cell + max_path_length
    i, j, d, D = neighbor_list('ijdD', atoms, cutoff=cutoff_serach_sphere)

    # make a ball of neighbors that is independent of chosen cell
    atoms_sphere = create_sphere_from_neighbor_list(
        atoms, el_ind,
        i=i, j=j, D=D)

    # el_ind in atoms_sphere became center atom and therefore index 0
    el_ind = 0

    # move through cell
    percolating, macro_vec, path_inds = move_through_cell(
        atoms_sphere, el_ind, i, j, D,
        max_path_length, longest_diagonal_cell,
        macro_vec=np.array([0, 0, 0], dtype=float),
        previous_move=np.array([0, 0, 0], dtype=float),
        path_inds=[0], tol=tol, verbose=verbose)

    # visualize just path without anything?
    if visualize:
        view(atoms_sphere)
        all_inds = [atom.index for atom in atoms_sphere]
        to_del = [e for e in all_inds if e not in path_inds]
        del atoms_sphere[to_del]
        view(atoms_sphere)

    return percolating, macro_vec


def move_through_cell(atoms, el_ind, i, j, D, max_path_length,
                      longest_diagonal_cell,
                      macro_vec=np.array([0, 0, 0], dtype=float),
                      previous_move=np.array([0, 0, 0], dtype=float),
                      path_inds=[0], tol=1, verbose=False):
    """ moves through cell and checks path until percolating is ensured

    Notes
    ----------
    Moves underly the following restrictions:
        1. never allow to go back to previous site
        2. never allow loops, that is revisit a site
        3. Only make moves that have "sane" paths tested using a check_path
        functino

    """
    atoms_sphere = atoms.copy()
    # now I do not need to worry about MIC anymore
    pos_moves, el_inds_moves = possible_moves_sphere(
        atoms_sphere, el_ind, max_path_length)

    # assumes this as starting position for circle check
    allowed_moves = []
    allowed_inds = []

    for move_vec in pos_moves:  # the starting moves
        # get path
        initial_p = macro_vec
        final_p = macro_vec + move_vec

        images, ind_final = get_path_for_move(
            atoms_sphere, el_ind, i, j, D,
            initial_p, final_p, move_vec)

        # check closest distances
        nebpath = NEBPath()
        sane = nebpath.check_path(images, tol=tol)
        if not sane:
            if verbose:
                print("Found non-percolating path")
            # path goes through bulk, cannot do that move
            continue

        # check if only possible move is to go back to itself!
        if np.linalg.norm(previous_move + move_vec) < 10e-10:
            if verbose:
                print("not going back to where I was")
            # just found the vector pointing back to where it came from
            continue

        # check if running in circles
        if ind_final in path_inds:
            if verbose:
                print("I'm in a cycle")
            continue

        allowed_moves.append(move_vec)
        allowed_inds.append(ind_final)

    if allowed_moves:
        # choose move that maximizes macro_vec distance (dont turn in circles)
        max_macro = -10
        for en, allowed_move in enumerate(allowed_moves):
            length_macro = np.linalg.norm(macro_vec + allowed_move)
            if length_macro > max_macro:
                max_macro = length_macro
                move_vec = allowed_move
                # set el_ind to final_ind to get new starting point
                el_ind = allowed_inds[en]

        # check goes out of unit cell
        macro_vec += move_vec
        path_inds.append(el_ind)
        if np.linalg.norm(macro_vec) > longest_diagonal_cell:
            if verbose:
                print(f"path is percolating with macrovector {macro_vec}")
            return True, macro_vec, path_inds

        # recursive function call until macrovector is larger than criterion
        return move_through_cell(
            atoms_sphere, el_ind, i, j, D, max_path_length,
            longest_diagonal_cell,
            macro_vec=macro_vec,
            previous_move=move_vec,
            path_inds=path_inds)

    else:
        # if it makes it until here, no move possible
        if verbose:
            print("No percolating paths possible from this position")
        return False, macro_vec, path_inds


def possible_moves_sphere(atoms_sphere, el_ind, max_path_length):
    """ Return possible moves for element within max_path_length cutoff sphere

    Parameters
    ------------
    atoms_sphere :  ase-atoms object
        created using ase neighbor list, has no cell shape
    el_ind : int
        Indice of start atom, used for extracting element string
    max_path_length : float
        Maximum allowed path distance

    Returns
    -------
    pos_moves : list of arrays
        Possible moves from el_ind to any neighboring atoms within
        max_path_length
    el_inds_move : list
        list of integers containing corresponding indice of final position atom

    """
    element = atoms_sphere[el_ind].symbol

    el_inds = [atom.index for atom in atoms_sphere if atom.symbol == element]
    info = atoms_sphere.get_distances(el_ind, el_inds, mic=False, vector=True)
    pos_moves = []
    el_inds_moves = []
    for en, dist_vec in enumerate(info):
        dist = np.linalg.norm(dist_vec)
        if dist < max_path_length and dist != 0:
            pos_moves.append(dist_vec)
            el_inds_moves.append(el_inds[en])

    return pos_moves, el_inds_moves


def get_edge_points_from_cell(cell):
    """ returns the edge points given the cell object """
    a, b, c = cell

    # lower plane points
    A = np.array([0, 0, 0])  # origin
    B = A + a
    C = A + b
    D = A + a + b
    # upper plane points
    E = A + c
    F = A + c + a
    G = A + c + b
    H = A + c + a + b

    return (A, B, C, D, E, F, G, H)


def get_longest_diagonal_cell(cell):
    """ Returns the longest diagonal possible in cell

    Parameters
    ----------
    cell : ndarray
        3D-array containing cell vectors a,b,c

    Returns
    -------
    dmax : float
        maximum diagonal in cell
    """
    A, B, C, D, E, F, G, H = get_edge_points_from_cell(cell)

    # 4 diagonals are possible
    diags = [[A, H], [B, G], [D, E], [C, H]]
    dmax = 0
    for p1, p2 in diags:
        d = np.linalg.norm(p2 - p1)
        if d > dmax:
            dmax = d
    return dmax


def get_path_for_move(atoms, el_ind, i, j, D,
                      initial_p, final_p, move_vec, N=11):
    """ Returns Neb Path for move

    Parameters
    ------------
    initial_p : array
        Initial position of atom path
    final_p : array
        Final position of atom in path
    move_vec : array
        vector connecting initial_p and final_p

    Returns
    --------
    images : ase-NEB path
        Images that are solely used for checking percolation
    final_ind : int
        Final indice of final position in path in atoms object. Note that this
        does not match the indices in the images, since a vacancy has been
        created in images!

    """
    # get path for this move
    # first which indices are involved
    atoms_sphere = atoms.copy()
    ind_final = get_ind_from_p(atoms_sphere, p=final_p)
    images = interpolate_path_sphere_linear(
        atoms_sphere, initial_p, final_p, move_vec, N=N)

    return images, ind_final


def interpolate_path_sphere_linear(atoms_sphere,
                                   initial_p,
                                   final_p,
                                   move_vec,
                                   N):
    """ Interpolates path linearly in sphere

    Notes
    ---------
    This wil return path is non-percolating for a linear intialization!
    Other initialization schemes might find paths that are indeed percolating.
    """
    ind_final = get_ind_from_p(atoms_sphere, p=final_p)
    ind_initial = get_ind_from_p(atoms_sphere, p=initial_p)
    # get vector connecting both points
    path_vec = final_p - initial_p
    final_p = initial_p + path_vec

    images = []
    for en in np.arange(0, N + 1, 1):
        image = atoms_sphere.copy()
        new_ps = image.get_positions()
        new_ps[ind_initial] = initial_p + (en/N)*path_vec
        image.set_positions(new_ps)
        # this delete can have impact on indices
        del image[ind_final]
        images.append(image)

    return images


def create_sphere_from_neighbor_list(atoms, ind, i, j, D):
    """ Return spehere atoms object from neighbor list """
    atoms = copy.deepcopy(atoms)

    syms = atoms.get_chemical_symbols()
    mask = i == ind

    points = [[0, 0, 0]]
    for d in D[mask]:
        points.append(d)

    symbols = [syms[ind]]
    symbols += [syms[i] for i in j[mask]]
    atoms_sphere = Atoms(symbols=symbols,
                         positions=points)
    return atoms_sphere


def get_ind_from_p(atoms, p):
    """ Get index from position """
    p = np.array(p)
    ps = atoms.get_positions()
    ind = np.argmin(np.linalg.norm(ps - p, axis=1))
    return ind


def get_non_percolating_structure():
    """ Get a non-percolating reference structure for Mg """
    from ase.build import bulk

    a3 = bulk('Cu', 'fcc', a=4.5, cubic=True)
    a3 = a3.repeat([2, 2, 2])

    syms = a3.get_chemical_symbols()
    syms[20] = 'Mg'
    syms[28] = 'Mg'
    ps = a3.get_positions()
    # move that one in the way
    ps[31] += np.array([-2.25, 0, 0])
    a3.set_chemical_symbols(syms)
    a3.set_positions(ps)

    # Path goes from 19 -> 27
    return a3


def get_circle_trapped_path_structure():
    """ Get a non-percolating reference structure for Mg """
    from ase.build import bulk

    a3 = bulk('Cu', 'fcc', a=4.5, cubic=True)
    a3 = a3.repeat([2, 2, 2])

    syms = a3.get_chemical_symbols()
    syms[10] = 'Mg'
    syms[28] = 'Mg'
    syms[17] = 'Mg'
    syms[7] = 'Mg'

    a3.set_chemical_symbols(syms)

    return a3


def test_all_iiw_structures():
    """ Test structures for iiw paper """
    from ase.db import connect
    db = connect('materials.db')

    mp_ids = [  # 'mp-676282',  # Chevrel
        # 'mp-27872',  # MgTi2O4
        # 'mp-18900',  # MgV2O4
        # 'mp-32006',  #MgMn2O4
        # 'mp-19541',  # Mg3Mn2O12Si3
        'mp-19581'  # Mg3Cr2O12Si3
    ]
    for mp_id in mp_ids:
        print(f"--------------- {mp_id} ------------------")
        atoms = db.get_atoms(material_id=mp_id)
        element = 'Mg'
        el_inds = [atom.index for atom in atoms if atom.symbol == element]

        for en, el_ind in enumerate(el_inds):
            print(f"------- ELEMENT INDEX {el_ind} --------------")
            info = check_element_trapped(atoms,
                                         max_path_length=5.5,
                                         el_ind=el_inds[0],
                                         tol=1,
                                         visualize=True)
            print("PATH percolating: ", info[0], info[1])
            break


def test_different_structures(test='chevrel'):
    """ Three different test structures available """
    from ase.io import read

    # there are three test cases:
    # Chevrel, handmade non-percolating and circle path
    atoms_no = get_non_percolating_structure()
    atoms_circle = get_circle_trapped_path_structure()

    # get supercell
    if test == 'chevrel':
        atoms_yes = read('chevrel_Mg2Mo4S8.cif')
    elif test == 'circle_trapped':
        atoms_yes = atoms_circle
    elif test == 'non_percolating':
        atoms_yes = atoms_no

    with open('potneb_dictionary.json', 'r') as f:
        dic = json.load(f)

    cutoff = dic['cutoff_neb_images']

    element = 'Mg'
    el_inds = [atom.index for atom in atoms_yes if atom.symbol == element]

    info = check_element_trapped(atoms_yes,
                                 max_path_length=cutoff,
                                 el_ind=el_inds[0],
                                 tol=1,
                                 visualize=True)
    print("OUTPUT: ", info)


final_ind = 26


def interacting(atoms, init_ind, final_ind, max_path_length, eps=1e-3):
    """
    Check if NEB path is accepted although path length not 2*vacancy spacing
    """
    atoms_temp = atoms.copy()
    path_length = atoms_temp.get_distances(init_ind, final_ind, mic=True)[0]
    to_delete = [atom.index for atom in atoms_temp if atom.index not in [
        init_ind, final_ind]]
    del atoms_temp[to_delete]
    atoms_temp.set_chemical_symbols(['Mg', 'Mg'])
    longest_diagonal_cell = get_longest_diagonal_cell(atoms_temp.cell)
    cutoff_serach_sphere = longest_diagonal_cell + max_path_length
    i, j, d, D = neighbor_list('ijdD', atoms_temp, cutoff=cutoff_serach_sphere)
    el_ind = 0
    mask = i == el_ind
    shortest_distance = min(d[mask])
    print("path length is: ", path_length)
    print("Closest atom to init is: ", shortest_distance)
    if shortest_distance + eps < path_length:
        print("INTERCATION HAPPENING - ABORT")


def test_interacting_NEB_path():
    """
    The vacancy spacing should be 2*NEB_path_length. That is to avoid spurious
    interaction between the final position in the NEB path with the initial
    image since those are connected by the NEB spring forces.
    In principal this choice is very robust. This function checks how far the
    final position is actually away from the next closest initial position. If
    the closest initial position is the NEB path length, it means the closest
    spring force it will interact is within the same path. If the next element
    is closer, that means it interacts with a different path and can therefore
    not be accepted!

    Note
    -----
    This assumes linear interpolation.
    This function should then be part of the inequivalent site finder. This
    function now only considers paths that are vacancy_spacing/2 long. This
    function shold allow it to check if there are paths that are longer, not
    violating the criteria that the path should be NEB_path_length apart from
    any other NEB path.
    """
    from ase.build import fcc111

    atoms = fcc111('Cu', [4, 4, 4], 4, periodic=True)
    max_path_length = 5
    init_ind = 21
    interacting(atoms, init_ind, final_ind, max_path_length)


if __name__ == "__main__":
    pass
    # test_all_iiw_structures()
    # test_different_structures(test='circle_trapped')
