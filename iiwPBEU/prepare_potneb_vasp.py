"""
Created on Fri Mar 29 08:42:56 2019

Prepare the structures for potential and NEB calculations

@author: Felix Bölle <feltbo@dtu.dk>
"""
import os
import json
import shutil

from ase.io import write

from iiwPBEU.system_utils import get_wd, cd
from iiwPBEU.inequivalent_site_finder import InequivalentSiteFinder
from iiwPBEU.battery_tools import get_relaxed_structure_from_folder,\
    get_relaxed_empty_from_folder,\
    get_required_unrelaxed_structures,\
    get_scaled_supercell,\
    get_correct_supercell_for_symmetry
from iiwPBEU.neb.check_reflective import is_reflective
from iiwPBEU.check_stability import read_from_check_stability_out
from iiwPBEU.neb.neb_path import NEBPath
from iiwPBEU.pre_screen import check_atom_percolating
import iiwPBEU.vasp_setups as vs
from iiwPBEU.input import UserInput


def rename_neb_path(path, new_name):
    folder_list = path.split('/')
    folder_list[-3] = new_name
    return '/'.join(folder_list)


def move_all_files(src, dest):
    """ Moves and overwrites existing files """
    files = os.listdir(src)
    for file in files:
        shutil.move('/'.join([src, file]), '/'.join([dest, file]))


def rename_NEB_folder(path, new_name='RMINEB'):
    """ Rename folder in case reflection symmetry apparent """
    dest = rename_neb_path(path, new_name=new_name)
    dest = get_wd(dest.split('/')[-5:])
    move_all_files(path, dest)
    return dest


def get_CIRNEB_folder(path):
    f_cirneb_list = path.split('/')[-5:]
    f_cirneb_list[-3] = 'CIRNEB'
    cirneb_p = get_wd(f_cirneb_list)
    return cirneb_p


def get_CICNEB_folder(path):
    f_cicneb_list = path.split('/')[-5:]
    f_cicneb_list[-3] = 'CICNEB'
    cicneb_p = get_wd(f_cicneb_list)
    return cicneb_p


def choose_reflective_path(all_sym_paths, take_out_n, n_ion_supercell,
                           supercell, supercell_empty,
                           logname=None):
    """
    Return only reflective paths from symmetry equiv paths

    Paramters
    ------------
    all_sym_paths : list of tuples
        symmetry equivalent paths as p=(init_ind, final_ind)

    Returns
    ------------
    start_ind, final_ind : int, int
        Indices of initial and final defect positions for NEB path
    """
    for start_ind, final_ind in all_sym_paths:
        res, sym, images = check_path_reflective(
            start_ind, final_ind, take_out_n, n_ion_supercell,
            supercell, supercell_empty, logname=logname)
        if res:
            return start_ind, final_ind
    # if no path is reflective, default choose first one
    start_ind, final_ind = all_sym_paths[0]
    return start_ind, final_ind


def check_path_reflective(start_ind, final_ind, take_out_n, n_ion_supercell,
                          supercell, supercell_empty,
                          logname=None, save_sym=False):
    """ Tests if given indice pair is a reflective path """
    # step 1 get unrelaxed defect structures
    sc_temp = supercell.copy()
    sc_empty_temp = supercell_empty.copy()
    init0, final0 = get_required_unrelaxed_structures(
        sc_temp, start_ind, final_ind, take_out_n, n_ion_supercell,
        supercell_empty=sc_empty_temp)
    # step 2 get relaxed supercell structure
    supercell_scaled = get_scaled_supercell(sc_temp, sc_empty_temp)
    sc = get_correct_supercell_for_symmetry(
        take_out_n, sc_temp, supercell_scaled)

    res, sym, images = is_reflective(
        sc, init0, final0, N=9, save_sym=save_sym, logname=logname)

    return res, sym, images


def get_ion_potential_folders(unique_ion_indices,
                              unique_ion_pairs,
                              atoms,
                              cut_off_neb,
                              path_tol,
                              take_out_n,
                              n_ion_supercell,
                              magstate,
                              functional,
                              pot_folders,
                              pot_folders_with_U):

    for unique_ion_ind in unique_ion_indices:
        # -------- check that percolating path exists! --------------------
        percolating = check_atom_percolating(
            atoms, cut_off_neb, unique_ion_ind, tol=path_tol)
        if not percolating:
            print(f"Ion ind {unique_ion_ind} not percolating")
            unique_ion_indices.remove(unique_ion_ind)
            # also remove from unique_pair_indices
            unique_ion_pairs = [e for e in unique_ion_pairs
                                if unique_ion_ind not in e]
            continue
        # -----------------------------------------------------------------
        pot_folders.append(get_wd(['potential', '{}_{}_{}'.format(
            take_out_n, n_ion_supercell, unique_ion_ind),
            magstate, 'vasp_rx_{}'.format(functional)]))

        pot_folders_with_U.append(get_wd(['potential', '{}_{}_{}'.format(
            take_out_n, n_ion_supercell, unique_ion_ind),
            magstate, 'vasp_rx_{}_U'.format(functional)]))

    return unique_ion_pairs, pot_folders, pot_folders_with_U


def get_neb_folders(unique_ion_pairs,
                    all_ion_pairs,
                    eq_to_ion_pairs,
                    take_out_n,
                    n_ion_supercell,
                    supercell,
                    supercell_empty,
                    magstate,
                    functional,
                    path_tol,
                    neb_folders,
                    path_reflective,
                    cneb_name='CNEB'):

    # NEB folders
    for unique_ion_pair in unique_ion_pairs:
        # --------------- reflective path check ---------------------------
        # get all symmetry equivalent paths to unique_ion_pair path
        all_sym_paths = [pair for i, pair in enumerate(all_ion_pairs)
                         if eq_to_ion_pairs[i] == unique_ion_pair]

        start_ind, final_ind = choose_reflective_path(
            all_sym_paths, take_out_n,
            n_ion_supercell, supercell, supercell_empty)
        # -----------------------------------------------------------------
        path = get_wd(['NEBs', '{}_{}_{}_{}'.format(
            start_ind, final_ind, take_out_n, n_ion_supercell),
            cneb_name, magstate, 'vasp_rx_{}'.format(functional)])

        with cd(path):
            # save symmetry information to path with this function call
            res, sym, images = check_path_reflective(
                start_ind, final_ind, take_out_n, n_ion_supercell,
                supercell, supercell_empty,
                logname='preparePot.log', save_sym=True)

            # also check if path is percolating
            sane = NEBPath.check_path(images, tol=path_tol)
            if not sane:
                msg = (f"Moving ion passes within {path_tol} Ang in  "
                       f"NEB path {start_ind}-{final_ind}")
                print(msg)
                continue
            # only append if it will be considered for calculation
            path_reflective.append(
                [[[start_ind, final_ind], take_out_n], res])

        if res:
            path = rename_NEB_folder(path, new_name='RMINEB')
            cirneb_p = get_CIRNEB_folder(path)
            neb_folders.append([path, cirneb_p])
        else:
            cicneb_p = get_CICNEB_folder(path)
            neb_folders.append([path, cicneb_p])
        # -----------------------------------------------------------------

    return neb_folders, path_reflective


class TaskPreparePotneb:
    """ Prepare potential and NEB calculations for the workflow """

    def __init__(self, path_tol=1, min_vac_spacing=8):
        self.path_tol = path_tol
        self.min_vac_spacing = min_vac_spacing

    def retrieve_information(self):
        with open('current_material.json') as f:
            user_args = json.load(f)
        return user_args

    def get_atoms_object(self, magstate):
        # get starting structure for symmetry inequivalent finder
        # Here we use the relaxed unit cell to do symmetry analysis on
        atoms = get_relaxed_structure_from_folder(0, magstate)
        # also get empty supercell for reflection check
        uc_empty = get_relaxed_empty_from_folder(0, magstate)

        return atoms, uc_empty

    def choose_vacancy_limits(self, user_args, n_ion_supercell):
        """ Create vacancy limits depending on user input """
        remove_n_ions = [1, n_ion_supercell - 1]

        try:
            remove = user_args['remove_n_ions']
            avail = UserInput.\
                schema_user_input['properties']['remove_n_ions']['enum']
            if remove == "single_one":
                remove_n_ions = [remove_n_ions[0]]
            if remove == "all_but_one":
                remove_n_ions = [remove_n_ions[1]]
            if remove == "both_charge_states":
                remove_n_ions = remove_n_ions
            if remove not in avail:
                msg = f"Specify in user input one of {avail}, not '{remove}'"
                raise NameError(msg)

        except KeyError:
            remove_n_ions = remove_n_ions

        ch_full, ch_empty = read_from_check_stability_out('.')
        ch_threshold = user_args["thresholds"]["ch_energy"]

        if ch_full > ch_threshold:
            print(f"""WARNING: ch criterion dilute vacancy limit cell exceeded
                  values are: {ch_full}/{ch_threshold} (eV/atom)""")
            if 1 in remove_n_ions:
                remove_n_ions.remove(1)

        if ch_empty > ch_threshold:
            print(f"""WARNING: ch criterion high vacancy limit cell exceeded
                  values are: {ch_empty}/{ch_threshold} (eV/atom)""")
            for val in remove_n_ions:
                if val != 1:
                    remove_n_ions.remove(val)

        if len(remove_n_ions) == 0:
            raise ValueError(""""No stable structures to calculate
                             Consider adjusting convex hull criterion""")

        return remove_n_ions

    def run(self):
        user_args = self.retrieve_information()

        # input for run
        material_id = user_args['material_id']
        magstate = user_args['magstate']
        ion = user_args['ion']
        cut_off_neb = user_args['cut_off_neb']
        functional = vs.get_functional()

        atoms, uc_empty = self.get_atoms_object(magstate)

        # create folder structure by creating starting structures first
        # function might CHANGE cut_off_neb if max number of atoms reached
        siteFinder = InequivalentSiteFinder(
            atoms=atoms,
            ion=ion,
            cutoff_neb_images=cut_off_neb,
            min_vac_spacing=self.min_vac_spacing)
        siteFinder.run()

        supercell = siteFinder.atoms_supercell
        supercell_empty = uc_empty.repeat(siteFinder.repeat)

        # how many ions should be taken out of the structure
        n_ion_supercell = supercell.get_chemical_symbols().count(ion)
        remove_n_ions = self.choose_vacancy_limits(user_args, n_ion_supercell)

        # create the folders
        neb_folders = []
        path_reflective = []
        pot_folders = []
        pot_folders_with_U = []
        for take_out_n in remove_n_ions:
            pot_info = get_ion_potential_folders(
                siteFinder.unique_ion_indices,
                siteFinder.unique_ion_pairs,
                atoms,
                cut_off_neb,
                self.path_tol,
                take_out_n,
                n_ion_supercell,
                magstate,
                functional,
                pot_folders,
                pot_folders_with_U
            )

            unique_ion_pairs, pot_folders, pot_folders_with_U = pot_info

            neb_info = get_neb_folders(
                siteFinder.unique_ion_pairs,
                siteFinder.all_ion_pairs,
                siteFinder.eq_to_ion_pairs,
                take_out_n,
                n_ion_supercell,
                supercell,
                supercell_empty,
                magstate,
                functional,
                self.path_tol,
                neb_folders,
                path_reflective
            )

            neb_folders, path_reflective = neb_info

        # check if only non-percolating paths found
        if len(neb_folders) == 0:
            raise ValueError("No percolating path found")

        self.store_output(siteFinder,
                          pot_folders,
                          pot_folders_with_U,
                          neb_folders,
                          path_reflective,
                          material_id,
                          cut_off_neb,
                          supercell,
                          )

    def store_output(self,
                     siteFinder,
                     pot_folders,
                     pot_folders_with_U,
                     neb_folders,
                     path_reflective,
                     material_id,
                     cut_off_neb,
                     supercell):
        # Save needed output in file
        # all the output needed will be saved once and for all,
        # so that calling the script multiple times will be avoided
        output_dic = {}
        output_dic['cutoff_neb_images'] = siteFinder.cutoff_neb_images
        output_dic['unique_ion_pairs'] = siteFinder.unique_ion_pairs
        output_dic['all_ion_pairs'] = siteFinder.all_ion_pairs
        output_dic['all_ion_indices'] = siteFinder.all_ion_indices
        output_dic['equal_to'] = siteFinder.eq_to
        output_dic['repeat_unitcell'] = siteFinder.repeat
        output_dic['pot_folders'] = pot_folders
        output_dic['pot_folders_with_U'] = pot_folders_with_U
        output_dic['neb_folders'] = neb_folders
        output_dic['is_reflective'] = path_reflective
        output_dic['eq_to_ion_pairs'] = siteFinder.eq_to_ion_pairs

        with open('potneb_dictionary.json', 'w') as f:
            json.dump(output_dic, f, indent=4)

        write('supercell_{}_{}.traj'.format(material_id, cut_off_neb),
              supercell)


if __name__ == "__main__":
    task = TaskPreparePotneb()
    task.run()
