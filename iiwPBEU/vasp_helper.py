# File for VASP specific functions that make life easier
from ase.io import write, iread
import numpy as np


def get_total_elastic_tensor_vasp(outcar_path):
    '''returns total elastic tensor C from VASP OUTCAR file'''
    elastic = list()
    append = False
    counter = 0
    with open(outcar_path, 'r') as f:
        for line in f:
            if line.rfind('TOTAL ELASTIC MODULI') > -1:
                append = True
            if append:
                if counter > 2:
                    # convert from (kbar) to (GPa)
                    elastic.append([float(x)/10 for x in line.split()[1:]])
                counter += 1
                if counter == 9:
                    return np.array(elastic)


def get_runtime_vasp(outcar_path):
    '''returns the Elapsed time of the job on the cluster -> time that it took
    until results are actually available.
    returns vasp time in s'''
    with open(outcar_path, 'r') as f:
        for line in f:
            if line.rfind('Elapsed time') > -1:
                return float(line.split()[-1])


def get_number_ionic_steps_vasp(outcar_path):
    '''Reads last iteration step number from OUTCAR file'''
    iterations = list()
    with open(outcar_path, 'r') as f:
        for line in f:
            if line.rfind('Iteration') > -1:
                iterations.append(line)
    # then only return last iteration number stored in OUTCAR file
    return int(iterations[-1].split()[2].split('(')[0])


def OUTCAR_to_traj(outcar_path, name='ase'):
    '''Writes ase.traj file from OUTCAR file'''
    write('{}.traj'.format(name), iread(outcar_path))
    return True
