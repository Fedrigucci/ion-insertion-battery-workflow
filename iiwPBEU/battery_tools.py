"""
Created on Tue Apr 30 2019

Frequently used functions for the ion-battery workflow

@author: Felix Bölle <feltbo@dtu.dk>
"""
import json
import glob
from pathlib import Path
import numpy as np
import os

from ase.io import read, iread, write
from ase.db import connect
from ase import Atom

import iiwPBEU.vasp_setups as vs

# ------------------------ I/O ------------------------------------------------


def get_starting_structure_from_folder(gobacknfolders, material,
                                       atoms_file_type='.cif'):
    """ Returns unrelaxed (MP) starting structure ase atoms object from path

    Parameters
    ----------
    gobacknfolders : int
        How far back in folder structure is starting_structures folder

    atoms_file_type : string
    File ending of atoms structure object. Default is .cif file

    Returns
    ----------
    atoms : ase-atoms object

    """

    # allow wild card
    path = '../'*gobacknfolders + 'starting_structures/{0}/{0}*{1}'.\
           format(material, atoms_file_type)
    full_filename = glob.glob(path)[0].split('/')[-1]  # last entry in path
    path = '/'.join(('/'.join(path.split('/')[:-1]), full_filename))

    return read(path)


def get_material_string_from_db(gobacknfolders, material_id):
    """ Return formula from database given the materialsproject ID """

    db = connect('./' + '../'*gobacknfolders +
                 'starting_structures/materials.db')
    row = db.get(material_id=material_id)
    material = row.formula

    return material


def get_starting_structure_from_db(gobacknfolders,
                                   material_id,
                                   with_u_corr=True):
    """ Returns unrelaxed (MP) starting structure ase atoms object from db

    Parameters
    ----------
    gobacknfolders : int
        How far back in folder structure is starting_structures database

    material : str
        Unit formula

    Returns
    ----------
    atoms : ase-atoms object

    hubbard : boolean
        True or False depending on user input.

    U_dic : dictionary
        Dictionary containing U-values used for material in materialsproject in
        ASE calculator friendly format

    """

    db = connect('./' + '../'*gobacknfolders +
                 'starting_structures/materials.db')
    row = db.get(material_id=material_id)
    atoms = row.toatoms()
    hubbard = with_u_corr
    if with_u_corr:
        U_dic = row.data['u_corr']
        ldauj = row.data['LDAUJ']
        ldaul = row.data['LDAUL']
        # convert to ASE calculator friendly dictionary
        for i, entry in enumerate(U_dic):
            U_dic[entry] = {'U': U_dic[entry],
                            'L': ldaul[i],
                            'J': ldauj[i]}
    else:
        U_dic = {}  # return an empty one

    return atoms, hubbard, U_dic


def get_relaxed_structure_from_folder(gobacknfolders, magstate,
                                      restart_f='ase-sort_0.dat',
                                      return_path=False):
    """ Returns relaxed structure ase atoms object from path

    Parameters
    ----------
    gobacknfolders : int
    How far back in folder structure is relaxation folder. The relaxation is
    then stored in /bulk/magstate/vasp_rx/atoms_filename . Looks for filename
    defined in vasp_setups.py function get_atoms_filename()

    Returns
    ----------
    atoms : ase-atoms object

    """
    functional = vs.get_functional()
    atoms_f = vs.get_atoms_filename()
    path = './' + '../'*gobacknfolders\
                + 'bulk/{}/vasp_rx_{}_U/'.format(magstate, functional)
    atoms = read(path + atoms_f)

    # get correct indexing in case VASP did something here
    if Path(path + restart_f).is_file():
        sort, sort_ase = get_vasp_restart_sort_list(path)
        atoms = atoms[sort_ase]
    else:
        sort, sort_ase = get_vasp_restart_sort_list(
            path, sortfile='ase-sort.dat')
        atoms = atoms[sort_ase]

    if return_path:
        return atoms, os.path.abspath(path)

    return atoms


def get_relaxed_empty_from_folder(gobacknfolders, magstate,
                                  restart_f='ase-sort_0.dat',
                                  atoms_f=None,
                                  return_path=False):
    """ Returns relaxed empty structure ase atoms object from path

    Parameters
    ----------
    gobacknfolders : int
    How far back in folder structure is relaxation folder. The relaxation is
    then stored in /bulk_empty/magstate/vasp_rx/atoms_filename . Looks for
    filename defined in vasp_setups.py function get_atoms_filename()

    Returns
    ----------
    atoms : ase-atoms object

    """
    functional = vs.get_functional()
    if not atoms_f:
        atoms_f = vs.get_atoms_filename()

    path = './' + '../'*gobacknfolders\
                + 'bulk_empty/{}/vasp_rx_{}_U/'.format(magstate, functional)
    atoms = read(path + atoms_f)
    # get correct indexing in case VASP did something here
    if restart_f:
        if Path(path + restart_f).is_file():
            sort, sort_ase = get_vasp_restart_sort_list(path)
            atoms = atoms[sort_ase]
        else:
            sort, sort_ase = get_vasp_restart_sort_list(
                path, sortfile='ase-sort.dat')
            atoms = atoms[sort_ase]
    if return_path:
        return atoms, os.path.abspath(path)

    return atoms


def get_relaxed_potential_structure_from_folder(gobacknfolders, n_mg, N_mg,
                                                unique_ind, magstate,
                                                restart_f='ase-sort_0.dat',
                                                atoms_f=None,
                                                return_path=False):
    """ Returns relaxed potential structure ase atoms object from path

    Parameters
    ----------
    gobacknfolders : int
    How far back in folder structure is relaxation folder. The relaxation is
    'potential/{}_{}_{}/{}/vasp_rx_{}/{}'.format(n_mg,N_mg,unique_ind,
    magstate, functional, atoms_f)) . Looks for whatever atom_f is set to!

    n_mg : int
    Amount of magnesium taken out of supercell.

    N_mg: int
    Maximum amount of magnesium in supercell

    unique_ind : int
    Indice of magnesium in supercell that was taken out of the structure

    magstate : str

    Returns
    ----------
    atoms : ase-atoms object

    """
    functional = vs.get_functional()
    if not atoms_f:
        atoms_f = vs.get_atoms_filename()
    # get one without +U for NEB calculations
    path = './' + '../'*gobacknfolders\
                + 'potential/{}_{}_{}/{}/vasp_rx_{}/'.format(n_mg, N_mg,
                                                             unique_ind,
                                                             magstate,
                                                             functional)
    atoms = read(path + atoms_f)
    # get correct indexing in case VASP did something here
    if restart_f:
        if Path(path + restart_f).is_file():
            sort, sort_ase = get_vasp_restart_sort_list(path)
            atoms = atoms[sort_ase]
        else:
            sort, sort_ase = get_vasp_restart_sort_list(
                path, sortfile='ase-sort.dat')
            atoms = atoms[sort_ase]
    if return_path:
        return atoms, os.path.abspath(path)

    return atoms


def get_supercell_from_folder(gobacknfolders):
    """ Returns supercell ase atoms object from path

    Parameters
    ----------
    gobacknfolders : int
    How far back in folder structure is supercell structure stored.

    Returns
    ----------
    atoms : ase-atoms object

    """

    # allow wild card
    path = './' + '../'*gobacknfolders + 'supercell*'
    full_filename = glob.glob(path)[0].split('/')[-1]  # last entry in path
    path = '/'.join(('/'.join(path.split('/')[:-1]), full_filename))

    return read(path)


def connect_to_ref_db(gobacknfolders):
    """ Connects to database containing reference energies for CH """
    path = './' + '../'*gobacknfolders + 'starting_structures/references_ch.db'
    db = connect(path)
    return db


def get_ion_ref_energy(gobacknfolders, ion):
    """ Return energy/atom of ion from reference database """
    db = connect_to_ref_db(gobacknfolders)
    row = db.get(elements=ion)
    e_per_atom = row.energy/len(row.toatoms())
    return e_per_atom


def get_current_material_dic(gobacknfolders):
    with open('./' + '../'*gobacknfolders + 'current_material.json') as f:
        dic = json.load(f)
    return dic


def get_vasp_restart_sort_list(path, sortfile='ase-sort_0.dat'):
    """ get the sorting of VASP compared to whatever it was before

    Parameters
    ----------
    init_file : str
        Filename of inital relaxation where everything was setup using ASE

    res_file : str
        Name of the restart file, if cannot find that one it means it has only
        restarted once and will simply read the OUTCAR file

    Notes
    --------
    That is necessary in cases where for example a vacancy defect creation
    destroys the normal ASE sorting.
    """

    with open(path + sortfile, 'r') as f:
        sort, sort_ase = [], []
        for line in f:
            s, s_ase = line.split()
            sort.append(int(s))
            sort_ase.append(int(s_ase))

    return sort, sort_ase

# -----------------------------------------------------------------------------

# --------------------------- manipulate structures ---------------------------


def reindex_atoms(atoms, sort_list):
    """ Reindex atoms according to sort_list, if not index list is wanted """

    atoms_temp = atoms.copy()
    del atoms_temp[[a.index for a in atoms_temp]]

    new_ord = zip(sort_list, atoms.get_chemical_symbols(),
                  atoms.get_positions())
    for ind, sym, p in sorted(new_ord):
        atoms_temp.append(Atom(symbol=sym, position=p))

    return atoms_temp


def OUTCAR_to_traj(outcar_path, name='ase'):
    '''Writes ase.traj file from OUTCAR file'''
    write('{}.traj'.format(name), iread(outcar_path))
    return True


def create_unique_defect(atoms_in, ion_ind, ion_out, ion_max, supercell=None):
    """
    Creates unique defect structures

    Parameters
    ----------
    atoms : ase-atoms object
    supercell structure containing all ions or none

    ion_ind : int
    This is the defect site that will either be removed or kept

    ion_out : int
    How many ions should be taken out of the supercell structure

    ion_max : int
    How many ions are inside the supercell structure

    supercell : atoms object
    If giving the empty supercell, also need to provide the ion filled supercell
    since we need to place ion back into the empty structure


    Returns
    -------
    atoms : ase-atoms object
    Created defect structure with ion_out amount ions taken out of the structure

    Notes
    -----------
    This function is only intended to create unique defects by taking either
    one ion out of the structure or all but one. That means that the
    restrictions on ion_out and ion_max are the two following:
        1. all but one ion taken out: ion_out + 1 == ion_max
        2. single ion taken out of structure: ion_out == 1

    """
    atoms = atoms_in.copy()  # dont touch that user input
    type = None

    if ion_out == 1:
        # take single ion out of structure
        del atoms[ion_ind]

    elif ion_out + 1 == ion_max:
        # step 1 get scaled filled supercell
        sc_copy = get_scaled_supercell(supercell, atoms)

        # step 2: take all but wanted ion out of structure
        ion_str = sc_copy.get_chemical_symbols()[ion_ind]
        rm_inds = [a.index for a in sc_copy if a.symbol == ion_str]
        rm_inds.remove(ion_ind)
        del sc_copy[rm_inds]
        atoms = sc_copy

    else:
        raise NotImplementedError("Method not intended to work for that set of"
                                  f" ion removal. You want to take {ion_out}"
                                  f"/{ion_max} ions out of the structure")

    if type == 'insertion':
        # place single ion back in empty structure
        ion_str = supercell.get_chemical_symbols()[ion_ind]
        # scaled position in supercell
        p_inter = supercell.get_scaled_positions()[ion_ind]
        # convert to empty supercell position
        p_inter = atoms.cell.cartesian_positions(p_inter)
        # now insert the new ion into the structure
        atoms.append(Atom(ion_str, p_inter))

    return atoms


def get_scaled_supercell(supercell, supercell_empty):
    """ Returns scaled supercell without any defect """

    sc_copy = supercell.copy()
    # atoms = empty supercell
    # supercell = bulk ion filled supercell
    # step 1: ratio in volume change empty/filled supercell
    v_empty = supercell_empty.get_volume()
    v_filled = sc_copy.get_volume()
    r = v_empty/v_filled

    # step 2: rescale supercell and atom positions
    scaled_cell = sc_copy.get_cell() * r**(1/3)
    sc_copy.set_cell(scaled_cell, scale_atoms=True)

    return sc_copy


def get_required_unrelaxed_structures(supercell,
                                      init_ind,
                                      final_ind,
                                      remove_n_ions,
                                      N_ions,
                                      supercell_empty=None):

    if remove_n_ions == 1:
        init0 = create_unique_defect(supercell,
                                     ion_ind=init_ind,
                                     ion_out=remove_n_ions,
                                     ion_max=N_ions)

        final0 = create_unique_defect(supercell,
                                      ion_ind=final_ind,
                                      ion_out=remove_n_ions,
                                      ion_max=N_ions)
    else:
        init0 = create_unique_defect(supercell_empty,
                                     ion_ind=init_ind,
                                     ion_out=remove_n_ions,
                                     ion_max=N_ions,
                                     supercell=supercell)

        final0 = create_unique_defect(supercell_empty,
                                      ion_ind=final_ind,
                                      ion_out=remove_n_ions,
                                      ion_max=N_ions,
                                      supercell=supercell)
    return init0, final0


def get_correct_supercell_for_symmetry(remove_n_ions,
                                       supercell,
                                       supercell_scaled):
    """ Returns Ion-filles supercell scaled or not scaled """
    if remove_n_ions == 1:
        sc_temp = supercell.copy()
    else:
        sc_temp = supercell_scaled.copy()
    return sc_temp

# -----------------------------------------------------------------------------

# ---------------- tools for calculating battery properties -------------------


def get_min_bar(images):
    """ Returns NEB barrier without fit

    Parameters
    ------------
    images : list of ase-atoms objects
    The converged images of the NEB path
    """
    Es = np.array([atoms.get_potential_energy() for atoms in images])
    Es = Es - Es[0]
    return max(Es)
