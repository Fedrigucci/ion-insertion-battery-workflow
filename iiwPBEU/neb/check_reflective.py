import json

from iiwPBEU.neb.rneb import RNEB
from iiwPBEU.neb.neb_path import NEBPath


def is_reflective(orig, init0, final0, align=True, N=5, save_sym=True,
                  tol=1e-3, logname=None):
    """
    Given unrelaxed initial and final structures it creates a path and
    checks whether it is reflection symmetric.
    The result (including the symmetries) is written to 'symmetry.json'.
    The created path is written to 'initial_path.traj'

    Parameters
    ----------
    orig: ASE atoms-object
        The original pristine supercell without any "defects"

    init0: ASE atoms-object
        The initial unrelaxed structure

    final0: ASE atoms-object
        The final unrelaxed structure

    align: boolean
        whether or not to align the indices

    N: integer
        Number of images

    save_sym: boolean
        whether or not to save the symmetries to file

    tol: float
        tolerance on the atomic position during symmetry analysis

    Return
    ---------
    res: boolean
        is the path reflective

    sym: nested lists
        The found reflection symmetries

    images: list of ASE-atoms objects
        The NEB path
    """
    neb_path = NEBPath(tol=tol, logname=logname)
    if align:
        final0, final = neb_path.align_indices(init0, final0)
    rneb = RNEB(tol=tol, logname=logname)
    sym = rneb.find_symmetries(orig, init0, final0)
    images = neb_path.create_path(init0, final0, N)

    sym = rneb.reflect_path(images, sym=sym)
    if len(sym) == 0:
        res = 0
    else:
        res = 1
        for i, s in enumerate(sym):
            for j, arr in enumerate(s):
                sym[i][j] = arr.tolist()
    if save_sym:
        save_result_to_json(res, sym=sym)

    return res, sym, images


def save_result_to_json(res, sym=None):
    output = {'symmetric': res,
              'symmetries': sym}
    with open('symmetry.json', 'w') as f:
        json.dump(output, f)
