#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 10:55:31 2019

Script for checking the potential of a structure for the ion-battery workflow

@authors: Alexander Juul Nielsen <s164009@student.dtu.dk>
          Felix Bölle <feltbo@dtu.dk>
"""
import os
import json

from ase.io import read

from iiwPBEU.system_utils import cd
import iiwPBEU.vasp_setups as vs
from iiwPBEU.battery_tools import get_ion_ref_energy
from iiwPBEU.data import n_val_ele

_calc_hf_nback = 3  # how far is calculations home folder back


def get_potential(relaxed_supercells, relaxed_bulk_unit, ion_removed, ion,
                  energy_ion):
    """ The function finds the change in potential from bulk unit cell
    to the supercell with one magnesium removed

    Parameters
    ----------

    relaxed_supercells : ase-atoms object
        list of relaxed supercells in ase atoms format

    relaxed_bulk_unit : ase-atoms object
        relaxed bulk unit cell with no magnesium removed in ase-atoms format

    ion_removed : int
        specifying the amount of magnesium removed from supercell

    energy_ion : float
        Energy per atom for ion (eV/atom)

    Returns
    ----------

    potential : float
        returns the change in potential energy [eV] from the bulk structure
        to the supercell with one magnesium removed

    Notes
    ----------
    Maybe return the amount of ions removed relative to total ions in the
    supercell? Maybe return voltage?

    """

    atoms = read(relaxed_supercells)
    pot_energy = atoms.get_potential_energy()

    # find minimum energy of the relaxed structures, since this is most likely
    bulk_atoms = read(relaxed_bulk_unit)
    bulk_energy = bulk_atoms.get_potential_energy()
    # Match stochiometry, since supercell is greater than unit cell
    N = (len(atoms)+ion_removed)/len(bulk_atoms)
    potential = (N*bulk_energy - (pot_energy + ion_removed*energy_ion))\
        / (n_val_ele[ion]*ion_removed)

    return potential


def get_minimum_potential_from_all_concs(magstate, ion, atoms_f):
    """ Returns the minimum potential based on all tested concentrations

    Parameters
    -----------
    magstate : string

    ion : str

    Returns
    -------
    min_pot : float
        Minimum potential based on all calculated concentrations.

    """
    functional = vs.get_functional()
    energy_ion = get_ion_ref_energy(_calc_hf_nback, ion)
    # now calculate the potential
    potentials = list()
    with cd('potential'):
        for folder in os.listdir():
            take_out_n, N, _ = folder.split('_')
            # get path to bulk atoms object
            OUTCAR_path_bulk = './../bulk/{}/vasp_rx_{}_U/{}'.format(
                magstate, functional, atoms_f)
            OUTCAR_path_pot = './{}/{}/vasp_rx_{}_U/{}'.format(
                folder, magstate, functional, atoms_f)
            potential = get_potential(relaxed_supercells=OUTCAR_path_pot,
                                      relaxed_bulk_unit=OUTCAR_path_bulk,
                                      ion_removed=int(take_out_n),
                                      ion=ion,
                                      energy_ion=energy_ion)
            potentials.append(potential)

    min_pot = min(potentials)

    return min_pot


def check_potential(atoms_f):
    # read information from current_material (magnetic states might change)
    with open('./current_material.json') as f:
        user_args = json.load(f)
        magstate = user_args['magstate']
    folder_info = os.getcwd()
    ion = folder_info.split('/')[-2]
    pot_threshold = user_args['thresholds']['potential']

    min_pot = get_minimum_potential_from_all_concs(magstate, ion, atoms_f)

    print("Potential minimum is:", min_pot, " eV")
    # crash if it does not match criterion
    if min_pot < pot_threshold:
        msg = ("Potential of material %.2f eV"
               " is lower than threshold %.2f eV" % (min_pot, pot_threshold))
        raise ValueError(msg)

    return min_pot


if __name__ == "__main__":
    atoms_f = vs.get_atoms_filename()
    pot = check_potential(atoms_f)
